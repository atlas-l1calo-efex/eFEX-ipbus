MP7_ROOT = $(shell pwd)/..

include  $(MP7_ROOT)/config/mfCommonDefs.mk

PACKAGE_VER_MAJOR = 1
PACKAGE_VER_MINOR = 4
PACKAGE_VER_PATCH = 0
PACKAGE_RELEASE = 0

ifndef BUILD_VERSION
BUILD_VERSION=1
endif

PYTHON_VERSION ?= $(shell python -c "import platform;print platform.python_version()")
PYTHON_INCLUDE_PREFIX ?= $(shell python -c "import distutils.sysconfig;print distutils.sysconfig.get_python_inc()")

export CACTUS_ROOT
export PACKAGE_VER_MAJOR PACKAGE_VER_MINOR PACKAGE_VER_PATCH

BINDINGS_SOURCES = $(wildcard src/common/*.cpp)
BINDINGS_OBJECT_FILES = $(patsubst src/common/%.cpp,obj/%.o,${BINDINGS_SOURCES})
BINDINGS_LIB = pkg/mp7/_core.so

ifdef EXTERN_BOOST_INCLUDE_PREFIX
# When compiling with the cactus makefile
MP7_MP7_PREFIX=${MP7_ROOT}/mp7/
MP7_MP7_INCLUDE_PREFIX = ${MP7_MP7_PREFIX}/include
MP7_MP7_LIB_PREFIX=${MP7_MP7_PREFIX}/lib/

INCLUDE_PATH = 	\
	-Iinclude  \
	-I${PYTHON_INCLUDE_PREFIX} \
	-I${MP7_MP7_INCLUDE_PREFIX} \
	-I${EXTERN_BOOST_INCLUDE_PREFIX} \
	-I${UHAL_LOG_INCLUDE_PREFIX} \
	-I${UHAL_UHAL_INCLUDE_PREFIX} \
	-I${UHAL_GRAMMARS_INCLUDE_PREFIX} \
	-I${CACTUS_ROOT}/include 

LIBRARY_PATH = \
	-L${EXTERN_BOOST_LIB_PREFIX} \
	-L${MP7_MP7_LIB_PREFIX} \
	-L${UHAL_UHAL_LIB_PREFIX} \
	-L${UHAL_LOG_LIB_PREFIX} \
	-L${UHAL_GRAMMARS_LIB_PREFIX} \
	-L${CACTUS_ROOT}/lib
else
# When compiling standalone
MP7_MP7_PREFIX=../mp7/
MP7_MP7_INCLUDE_PREFIX = ${MP7_MP7_PREFIX}/include
MP7_MP7_LIB_PREFIX=${MP7_MP7_PREFIX}/lib/

INCLUDE_PATH = \
	-Iinclude  \
	-I${PYTHON_INCLUDE_PREFIX} \
	-I${MP7_MP7_INCLUDE_PREFIX} \
	-I${CACTUS_ROOT}/include 

LIBRARY_PATH = \
	-L${MP7_MP7_LIB_PREFIX} \
	-L${CACTUS_ROOT}/lib
endif
# uhal dependencies. needed?


BINDING_LIBRARIES = 	\
			-lpthread \
			-ldl \
			-lutil \
			\
			-lboost_filesystem \
			-lboost_python \
			-lboost_regex \
			-lboost_system \
			-lboost_thread \
			\
			-lcactus_uhal_log \
			-lcactus_uhal_uhal \
			-lcactus_mp7_mp7 

#-lcactus_extern_pugixml 

			#-lcactus_uhal_grammars \
			#-lcactus_uhal_log \
			#-lcactus_uhal_uhal

CPP_FLAGS = -g -Wall -MMD -MP -fPIC ${INCLUDE_PATH}

LINK_BINDINGS_FLAGS = -shared -fPIC -Wall -g -Wl,-h -Wl,-Bstatic -Wl,-Bdynamic ${LIBRARY_PATH}  ${BINDING_LIBRARIES}

.PHONY: all _all clean _cleanall build _buildall install _installall rpm _rpmall test _testall spec_update generator

default: build

clean: _cleanall
_cleanall:
	rm -rf ${RPMBUILD_DIR}
	rm -rf obj
	rm -rf ${BINDINGS_LIB}
	rm -rf build dist MANIFEST

all: _all
build: _all
buildall: _all
_all: ${BINDINGS_LIB}	

#Library will be compiled and will be packaged
${BINDINGS_LIB}: ${BINDINGS_OBJECT_FILES}
	g++ ${LINK_BINDINGS_FLAGS} ${BINDINGS_OBJECT_FILES} -o $@

${BINDINGS_OBJECT_FILES}: obj/%.o : src/common/%.cpp
	mkdir -p obj
	g++ -c ${CPP_FLAGS} -ftemplate-depth-128 -O0 -rdynamic -finline-functions -Wno-inline -DNDEBUG  $< -o $@	

-include $(BINDINGS_OBJECT_FILES:.o=.d)	

rpm: _rpmall
_rpmall: ${BINDINGS_LIB}
	rm -rf /var/tmp/cactus*
	python setup.py bdist_rpm --release ${PACKAGE_RELEASE}.${CACTUS_OS}.python${PYTHON_VERSION} --binary-only --force-arch=`uname -m`


