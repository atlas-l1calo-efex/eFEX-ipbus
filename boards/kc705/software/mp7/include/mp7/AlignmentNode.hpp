/**
 * @file    AlignmentNode.hpp
 * @author  Alessandro Thea
 * @brief   Brief description
 * @date    30/08/2014
 * 
 */

#ifndef _mp7_AlignmentNode_hpp_
#define	_mp7_AlignmentNode_hpp_

// uHAL Headers
#include "uhal/DerivedNode.hpp"

namespace mp7 {
    /*!@class AlignmentNode
     * @brief Specialized node to control the channel alignment mechanism
     * 
     * To fill
     * 
     * @author Alessandro Thea
     * @date  2013
     */

    class AlignmentNode : public uhal::Node {
        UHAL_DERIVEDNODE( AlignmentNode );
    public:

        // PUBLIC METHODS
        AlignmentNode( const uhal::Node& aNode );
        virtual ~AlignmentNode( );

        /// Align and wait for confirmation
        void align( bool force=false ) const;

        /// Disable the channels
        void enable( std::vector<bool> channels ) const;

        /// Check alignment and CRCs
        bool check( ) const;

    };
}

#endif	/* _mp7_AlignmentNode_hpp_ */


