/**
 * @file    MiniPODMasterNode.hpp
 * @author  Alessandro Thea
 * @brief   Brief description
 * @date    30/08/2014
 **/

#ifndef MP7_MINIPODMASTERNODE_HPP
#define	MP7_MINIPODMASTERNODE_HPP

#include "mp7/I2CMasterNode.hpp"

// Temporary inclusion
#include "mp7/MiniPODNode.hpp"

namespace mp7 {

// Forward declarations
class MiniPODRxSlave;
class MiniPODTxSlave;

/**
 * @class MiniPODMasterNode
 */
class MiniPODMasterNode : public opencores::I2CBaseNode {
    UHAL_DERIVEDNODE(MiniPODMasterNode);

public:

    MiniPODMasterNode(const uhal::Node& aNode);

    MiniPODMasterNode(const MiniPODMasterNode& aOther);

    virtual ~MiniPODMasterNode();

    const MiniPODRxSlave& getRxPOD(const std::string& name) const;
    const MiniPODTxSlave& getTxPOD(const std::string& name) const;

    std::vector<std::string> getRxPODs() const;
    std::vector<std::string> getTxPODs() const;

protected:
    boost::unordered_map<std::string, MiniPODRxSlave*> mRxPODs;
    boost::unordered_map<std::string, MiniPODTxSlave*> mTxPODs;
private:
    /// Construct the object from uhal::Node
    void constructor();
};

/**
 * @class MiniPODSlave
 */
class MiniPODSlave : public opencores::I2CSlave {
public:
    MiniPODSlave(const opencores::I2CBaseNode* aMaster, uint8_t aSlaveAddress);
    virtual ~MiniPODSlave();

    virtual Measurement get3v3();
    virtual Measurement get2v5();
    virtual Measurement getTemp();
    virtual Measurement getOnTime();
    virtual std::vector < Measurement > getOpticalPowers();

    virtual void setChannelPolarity(const uint32_t& aMask);
    virtual void disableChannel(const uint32_t& aMask);
    virtual void disableSquelch(const bool& aDisabled);

    virtual std::pair< bool, bool > getAlarmTemp();
    virtual std::pair< bool, bool > getAlarm3v3();
    virtual std::pair< bool, bool > getAlarm2v5();
    virtual std::vector< bool > getAlarmLOS();
    virtual std::vector< std::pair< bool, bool > > getAlarmOpticalPower();

    virtual MiniPODinfo getInfo();

protected:
    uint16_t getUint16(const uint32_t& aMSB, const uint32_t& aLSB);
    std::vector<uint8_t> block_read(const uint32_t& aI2CbusAddress, const uint32_t aSize);

};

/**
 * @class MiniPODRxSlave
 */
class MiniPODRxSlave : public MiniPODSlave {
public:
    MiniPODRxSlave(const opencores::I2CBaseNode* aMaster, uint8_t aSlaveAddress);
    virtual ~MiniPODRxSlave();
    
    void setDeemphasis ( const double& aPercentage );
    void setOutputAmplitude ( const double& aPercentage );
private:

};

class MiniPODTxSlave : public MiniPODSlave {
public:
    MiniPODTxSlave(const opencores::I2CBaseNode* aMaster, uint8_t aSlaveAddress);
    virtual ~MiniPODTxSlave();
    
    virtual std::vector < Measurement > getBiasCurrents();

    virtual void setInputEqualization ( const double& aPercentage );

    virtual void marginMode ( const bool& aEnabled );

    virtual std::vector< bool > getAlarmFault();
    virtual std::vector< std::pair< bool , bool > > getAlarmBiasCurrent();
private:

};



}

#endif	/* MP7_MINIPODMASTERNODE_HPP */

