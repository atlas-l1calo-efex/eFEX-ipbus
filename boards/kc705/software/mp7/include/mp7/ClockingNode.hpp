#ifndef _mp7_ClockingNode_hpp_
#define	_mp7_ClockingNode_hpp_

// MP7 Headers
#include "uhal/DerivedNode.hpp"

// C++ Headers
#include <map>

namespace mp7 {

/*!
 * @class XpointNode
 * @brief Specialised Node to control the Xpoint switches
 *
 * @author Alessandro Thea
 * @date August 2013
 */



class ClockingNode : public uhal::Node {
    UHAL_DERIVEDNODE(ClockingNode);
public:

    enum Clk40Select {
        ExternalAMC13,
        ExternalMCH,
        Disconnected
    };

    enum RefClkSelect {
        Oscillator,
        ClockCleaner
    };

    // PUBLIC METHODS
    ClockingNode(const uhal::Node&);
    virtual ~ClockingNode();

    /// Configure the routing by logical states
    void configure(const std::string aConfig) const;

    /// Configure the routing by logical states
    void configureXpoint(Clk40Select aClk40Src, RefClkSelect aRefSrc) const;

    /// Configure the U3 switch
    void configureU3(uint SelForOut0, uint SelForOut1, uint SelForOut2, uint SelForOut3) const;

    /// Configure the U15 switch
    void configureU15(uint SelForOut0, uint SelForOut1, uint SelForOut2, uint SelForOut3) const;

    /// Configure the U36 switch
    void configureU36(uint SelForOut0, uint SelForOut1, uint SelForOut2, uint SelForOut3) const;

    /// Reset the SI5326
    void si5326Reset() const;

    // Wait for SI5326 configuration to complete
    void si5326WaitConfigured(uint32_t aMaxTries = 1000) const;

    /// Check the SI5326 loss of lock
    bool si5326LossOfLock() const;

    /// What is this?
    bool si5326Interrupt() const;

protected:

    // PROTECTED METHODS
    void configureUX(const std::string& chip, uint SelForOut0, uint SelForOut1, uint SelForOut2, uint SelForOut3) const;

private:
    struct Config {
        Clk40Select clk40;
        RefClkSelect refClk;
        std::string si5626Cfg;
    };
    
    typedef boost::unordered_map<std::string,Config> ConfigMap;
    
    //! Map of the known configurations
    static const ConfigMap mConfigurations;
    
    //! Method to generate the default configurations
    static ConfigMap makeDefaultConfigs();
};


}



#endif	/* _mp7_ClockingNode_hpp_ */


