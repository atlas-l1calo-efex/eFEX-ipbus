#ifndef _mp7_CSRNode_hpp_
#define	_mp7_CSRNode_hpp_

// MP7 Headers
#include "uhal/DerivedNode.hpp"

namespace mp7 {
    /*!
     * @class CtrlNode
     * @brief Specialised node that provides the basic control over the MP7 board
     * 
     * Some notes about the registers
     * Tx/Rx buffer selection: rx = 0,  tx = 1 (as in BufferSelect enum)
     * Region Info register:
     * 0 = empty
     * 1 = buffer only
     * 2 = gth_10g
     * 
     * @author Alessandro Thea
     * @date  2013
     */

    class CtrlNode : public uhal::Node {
        UHAL_DERIVEDNODE( CtrlNode );
        
    public:
      
        enum BufferSelection { Rx, Tx };
        
        enum RegionType { RegEmpty = 0, RegBuffer = 1, RegMGT = 2 };

        /*!
         * @class Clock40Sentry
         * @brief Class to release
         */
        class Clock40Guard {
        public:
            Clock40Guard( const CtrlNode& aCtrl, double aMilliSleep=1000 );

            virtual ~Clock40Guard();
            
        private:
            const CtrlNode& mCtrl;
            bool mReset;
            double mMilliSleep;
            friend class CtrlNode;
        };
        
        // PUBLIC METHODS
        CtrlNode( const uhal::Node& );
        virtual ~CtrlNode( );

        /// Nuke the board and reset the 40 Mhz clock
        void hardReset( double aMilliSleep=1000 ) const;
        
        /// Soft reset
        void softReset() const;
        
        /// Reset Clock 40
        void resetClock40( bool aReset=true ) const;
        
        /// Return the locking status of the 40Mhz clock
        bool clock40Locked( ) const;
        
        /// wait (up to 100 ms) for the clock lock
        void waitClk40Lock( uint32_t aMaxTries=1000 ) const;

        /// Select the clock
        void selectClk40Source( bool aExternalClock=true, double aMilliSleep=1000 ) const;

        /// Select the channel to access to
        void selectChannel( uint32_t channel ) const;

        /// Select quad to access to
        void selectRegion( uint32_t quad ) const;

        /// Select quad and channel
        void selectRegChan( uint32_t quad, uint32_t chan ) const;

        /// Select link
        void selectLink( uint32_t link ) const;
        
        /// Select buffer
        void selectLinkBuffer( uint32_t aLink, BufferSelection aBuffer ) const;

        /// Returns the overall list of regions
        std::vector<uint32_t> getRegions() const;
        
        /// Returns the list of regions with buffers
        std::vector<uint32_t> getBufferRegions() const;
        
        /// Returns the list of regions with mgts
        std::vector<uint32_t> getMGTRegions() const;
        
        /// 
        boost::unordered_map<std::string, uint32_t> getGenerics() const;
        /*
        /// configure the clocking infrastructure
         void configureClocking( ) const;
        */

    };

}

#endif	/* _mp7_CSRNode_hpp_ */

