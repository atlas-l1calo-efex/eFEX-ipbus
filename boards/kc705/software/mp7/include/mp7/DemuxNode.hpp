#ifndef _mp7_DemuxNode_hpp_
#define	_mp7_DemuxNode_hpp_

// MP7 Headers
#include "uhal/DerivedNode.hpp"

namespace mp7 {
    /*!
     * @class DemuxNode
     * @brief
     * @author Alessandro Thea
     * @date 2013
     */

    class DemuxNode : public uhal::Node {
        UHAL_DERIVEDNODE( DemuxNode )
    public:
        DemuxNode( const uhal::Node& node );
        virtual ~DemuxNode( );
    private:

    };

}

#endif	/* _mp7_DemuxNode_hpp_ */


