/**
 * @file    SI570Node.cpp
 * @author  Alessandro Thea
 * @brief   Brief description
 * @date 
 */


#ifndef MP7_SI570NODE_HPP
#define	MP7_SI570NODE_HPP

#include "mp7/OpenCoresI2C.hpp"
#include "mp7/I2CMasterNode.hpp"

namespace mp7 {
    
class SI570Node : public OpenCoresI2C {
    UHAL_DERIVEDNODE( SI570Node );
public:
    SI570Node( const uhal::Node& aNode );
    SI570Node( const uhal::Node& aNode, uint8_t aAddr );
    virtual ~SI570Node();

    void configure(const std::string& aFilename) const;

private:

};


class SI570Slave : public opencores::I2CSlave {
public:
    SI570Slave( const opencores::I2CBaseNode* aMaster, uint8_t aSlaveAddress );
    virtual ~SI570Slave();

    void configure(const std::string& aFilename) const;
};



class SI570Node2g : public opencores::I2CBaseNode, public SI570Slave {
    UHAL_DERIVEDNODE( SI570Node2g );
public:
    SI570Node2g( const uhal::Node& aNode );
    SI570Node2g( const SI570Node2g& aOther );
    virtual ~SI570Node2g();

};
}
#endif	/* MP7_SI570NODE_HPP */

