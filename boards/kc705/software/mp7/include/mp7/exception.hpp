/*
 * File:   exception.h
 * Author: ale
 *
 * Created on August 21, 2013, 2:29 PM
 */

#ifndef __mp7_exception_h__
#define	__mp7_exception_h__

#include "uhal/log/exception.hpp"

namespace mp7 {
    namespace exception {
        ExceptionClass( MP7HelperException, "Exception class to handle MP7 specific exceptions" );
        ExceptionClass( Clock40NotInReset, "Exception class to handle cases where the clock source is change without keeping the line in reset" );
        ExceptionClass( Clock40LockFailed, "Exception class to handle failure of clock 40 locking" );
        ExceptionClass( XpointConfigTimeout, "Exception class to handle Xpoint configuration timeout" );
        ExceptionClass( SI5326ConfigurationTimeout, "Exception class to handle si5326 configuration timeout" );

        ExceptionClass( BC0LockFailed, "Exception class to handle failure of BC0 lock" );
        ExceptionClass( TTCFrequencyInvalid, "Exception class to handle TTC invalid frequency readings" );

        ExceptionClass( GTHFSMResetTimeout, "Exception class to handle failure to reset GTH's fsms");
        
        ExceptionClass( BufferLockFailed, "Exception class to handle failure of Buffer lock" );
        ExceptionClass( BufferConfigError, "Exception class to handle buffer misconfiguration" );
        ExceptionClass( BufferSizeExceeded, "Exception class to handle buffer size errors" );
        ExceptionClass( I2CSlaveNotFound, "Exception class to handle missing I2C slaves" );
        ExceptionClass( I2CException, "Exception class to handle I2C Exceptions" );
    }
}

#endif	/* __mp7_exception_h__ */


