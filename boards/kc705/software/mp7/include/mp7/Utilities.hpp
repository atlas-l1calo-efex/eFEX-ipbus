#ifndef __mp7_utilities_hpp__
#define __mp7_utilities_hpp__

// C++ Headers
#include <string>
#include <istream>
#include <stdint.h>
#include <stdlib.h>


// Boost Headers
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_signed.hpp>
#include <boost/type_traits/is_unsigned.hpp>
#include <boost/unordered_map.hpp>

// uHAL Headers
#include <uhal/Node.hpp>

namespace mp7 {

// Wrappers to be used by lexical_cast
template < typename T > struct stol;
template < typename T > struct stoul;

//! Walk & read the node structure.
boost::unordered_map<std::string, uint32_t> snapshot(const uhal::Node& aNode);


/**
 * Sleeps for a given number of milliseconds
 * @param aTimeInMilliseconds Number of milliseconds to sleep
 */
void millisleep(const double& aTimeInMilliseconds);

/**
 * Formats a std::string in printf fashion
 * @param fmt Format string
 * @param ... List of arguments
 * @return A formatted string
 */
std::string strprintf(const char* aFmt, ...);

std::vector<std::string> shellExpandPaths(const std::string& aPath);

std::string shellExpandPath(const std::string& aPath);

}

#include "mp7/Utilities.hxx"

#endif /* _mp7_helpers_hpp_ */

