/**
 * @file    SI5326Node.hpp
 * @author  Alessandro Thea
 * @brief   Brief description
 * @date 
 */
#ifndef MP7_SI5326_HPP
#define	MP7_SI5326_HPP

#include "mp7/OpenCoresI2C.hpp"
#include "mp7/I2CMasterNode.hpp"

namespace mp7 {
    /*!
     * @class SI5326Node
     * @brief
     * @author Alessandro Thea
     * @date August 2013
     */

    class SI5326Node : public OpenCoresI2C {
        UHAL_DERIVEDNODE( SI5326Node );
    public:
        SI5326Node( const uhal::Node& aNode );
        SI5326Node( const uhal::Node& aNode, uint8_t aAddr );
        virtual ~SI5326Node( );

        void configure( const std::string& aFilename ) const;
        void reset( ) const;
        void intcalib( ) const;
        void sleep( const bool& s ) const;
        void debug( ) const;

        std::map<uint32_t, uint32_t> registers( ) const;
    private:

        static const uint8_t m_slaveAddress;

    };

    class SI5326Slave : public opencores::I2CSlave {
    public:
        SI5326Slave( const opencores::I2CBaseNode* aMaster, uint8_t aSlaveAddress );
        virtual ~SI5326Slave( );

        void configure( const std::string& aFilename ) const;
        void reset( ) const;
        void intcalib( ) const;
        void sleep( const bool& s ) const;
        void debug( ) const;

        std::map<uint32_t, uint32_t> registers( ) const;

    };

    class SI5326Node2g : public opencores::I2CBaseNode, public SI5326Slave {
        UHAL_DERIVEDNODE( SI5326Node2g );
    public:
        SI5326Node2g( const uhal::Node& aNode );
        SI5326Node2g( const SI5326Node2g& aOther );
        virtual ~SI5326Node2g();

};

}

#endif	/* MP7_SI5326_HPP */


