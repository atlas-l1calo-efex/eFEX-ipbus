#ifndef _mp7_ClockingXENode_hpp_
#define	_mp7_ClockingXENode_hpp_

// MP7 Headers
#include "uhal/DerivedNode.hpp"

// C++ Headers
#include <map>

namespace mp7 {

/*!
 * @class ClockingXENode
 * @brief Specialised Node to control the Xpoint switches
 *
 * @author Alessandro Thea
 * @date August 2013
 */


class ClockingXENode : public uhal::Node {
    UHAL_DERIVEDNODE(ClockingXENode);
public:

    enum Clk40Select {
        ExternalAMC13,
        ExternalMCH,
        Disconnected
    };

//    enum RefClkSelect {
//        Oscillator,
//        ClockCleaner
//    };

    // PUBLIC METHODS
    ClockingXENode(const uhal::Node&);
    virtual ~ClockingXENode();
    
    /// Configure the routing by logical states
    void configure(const std::string aConfig) const;

    /// Configure the routing by logical states
    void configureXpoint(Clk40Select aClk40Src) const;

    /// Configure the U36 switch
    void configureU36(uint SelForOut0, uint SelForOut1, uint SelForOut2, uint SelForOut3) const;

    /// Reset the SI5326
    void si5326TopReset() const;

    // Wait for SI5326 configuration to complete
    void si5326TopWaitConfigured(bool aMustLock = true, uint32_t aMaxTries = 1000) const;

    /// Check the SI5326 loss of lock
    bool si5326TopLossOfLock() const;

    /// What is this?
    bool si5326TopInterrupt() const;

    /// Reset the SI5326
    void si5326BottomReset() const;

    // Wait for SI5326 configuration to complete
    void si5326BottomWaitConfigured(bool aMustLock = true, uint32_t aMaxTries = 1000) const;

    /// Check the SI5326 loss of lock
    bool si5326BottomLossOfLock() const;

    /// What is this?
    bool si5326BottomInterrupt() const;

protected:

    // PROTECTED METHODS
    void configureUX(const std::string& chip, uint SelForOut0, uint SelForOut1, uint SelForOut2, uint SelForOut3) const;

    /// Reset the SI5326
    void si5326ChipReset(const std::string& aChip) const;

    // Wait for SI5326 configuration to complete
    void si5326ChipWaitConfigured(const std::string& aChip, bool aMustLock = true, uint32_t aMaxTries = 1000) const;

    /// Check the SI5326 loss of lock
    bool si5326ChipLossOfLock(const std::string& aChip) const;

    /// What is this?
    bool si5326ChipInterrupt(const std::string& aChip) const;
private:

    struct Config {
        Clk40Select clk40;
        std::string si570Cfg;
        std::string si5626TopCfg;
        std::string si5626BotCfg;
    };

    typedef boost::unordered_map<std::string, Config> ConfigMap;

    //! Map of the known configurations
    static const ConfigMap mConfigurations;
    
        //! Method to generate the default configurations
    static ConfigMap makeDefaultConfigs();
};


}



#endif	/* _mp7_ClockingXENode_hpp_ */


