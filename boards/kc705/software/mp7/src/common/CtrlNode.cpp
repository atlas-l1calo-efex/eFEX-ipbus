#include "mp7/CtrlNode.hpp"

// MP7 Headers
#include "mp7/Utilities.hpp"
#include "mp7/exception.hpp"
#include <mp7/Utilities.hpp>

// uHal Headers
#include "uhal/log/log.hpp"

// Boost Headers
#include <boost/iterator/counting_iterator.hpp>
#include <boost/iterator/zip_iterator.hpp>
#include <boost/foreach.hpp>

// Namespace resolution
using namespace std;
using namespace uhal;

namespace mp7 {

UHAL_REGISTER_DERIVED_NODE(CtrlNode);

// PUBLIC METHODS

CtrlNode::CtrlNode(const uhal::Node& node) : uhal::Node(node) {
}

CtrlNode::~CtrlNode() {
}

void
CtrlNode::hardReset(double aMilliSleep) const {
    getNode("csr.ctrl.nuke").write(0x1);
    getClient().dispatch();
    millisleep(aMilliSleep);
}

void
CtrlNode::softReset() const {
    getNode("csr.ctrl.soft_rst").write(0x1);
    getClient().dispatch();
}

void
CtrlNode::resetClock40(bool aReset) const {
    getNode("csr.ctrl.clk40_rst").write((int)aReset);
    getClient().dispatch();
}

void
CtrlNode::selectClk40Source(bool aExternalClock, double aMilliSleep) const {
    uhal::ValWord<uint32_t> isreset = getNode("csr.ctrl.clk40_rst").read();
    getClient().dispatch();
    
    if ( !isreset.value() ) {
        exception::Clock40NotInReset lExc;
        log(lExc, "Cannot change the source if Clock 40 is not in reset)");
//        throw lExc;
        cout << "Clock40 is not in reset!" << isreset.value() << endl;
    }
    
    getNode("csr.ctrl.clk40_sel").write((int) aExternalClock);
    getClient().dispatch();

    
}


bool
CtrlNode::clock40Locked() const {
    uhal::ValWord<uint32_t> lock = getNode("csr.stat.clk40_lock").read();
    getClient().dispatch();
    return  lock.value();
}

void
CtrlNode::waitClk40Lock(uint32_t aMaxTries) const {
    uhal::ValWord< uint32_t > clk40_lck(0);
    int32_t countdown(aMaxTries);

    while (countdown > 0) {
        clk40_lck = this->getNode("csr.stat.clk40_lock").read();
        this->getClient().dispatch();
        if (clk40_lck.value()) {
            break;
        }

        millisleep(1);
        countdown--;
    }

    if (countdown == 0) {
        //throw runtime_error("timed out waiting for clock40 lock signal");
        exception::Clock40LockFailed lExc;
        log(lExc, "Timed out while waiting for Clock40 to lock (", uhal::Integer(aMaxTries), " ms)");
        throw lExc;
    } else {
        log(Notice(), "Clock 40 Locked after ", uhal::Integer(aMaxTries - countdown), " ms");
    }
}

void
CtrlNode::selectChannel(uint32_t channel) const {
    // throw here if channel > 4
    getNode("csr.ctrl.chan_sel").write(channel);
    getClient().dispatch();
}

void
CtrlNode::selectRegion(uint32_t region) const {
    //
    getNode("csr.ctrl.quad_sel").write(region);
    getClient().dispatch();
}

void
CtrlNode::selectRegChan(uint32_t quad, uint32_t chan) const {
    getNode("csr.ctrl.quad_sel").write(quad);
    getNode("csr.ctrl.chan_sel").write(chan);
    getClient().dispatch();
}

void
CtrlNode::selectLink(uint32_t link) const {
    log(Debug(), "Selecting link ", uhal::Integer(link));
    this->selectRegChan(link / 4, link % 4);
}

void
CtrlNode::selectLinkBuffer(uint32_t aLink, BufferSelection aBuffer) const {
    log(Debug(), "Selecting link buffer ", uhal::Integer(aLink));
    getNode("csr.ctrl.quad_sel").write(aLink / 4);
    getNode("csr.ctrl.chan_sel").write(aLink % 4);
    getNode("csr.ctrl.txrx_sel").write(aBuffer);
    getClient().dispatch();
}

/// returns the overall list of regions

std::vector<uint32_t>
CtrlNode::getRegions() const {
    uhal::ValWord<uint32_t> n_region = this->getNode("id.generics.n_region").read();
    this->getClient().dispatch();

    return vector<uint32_t>(boost::counting_iterator<uint32_t>(0), boost::counting_iterator<uint32_t>(n_region.value()));
}

std::vector<uint32_t>
CtrlNode::getBufferRegions() const {

    vector<uint32_t> regions = this->getRegions();
    
    // Queue region info read requests
    vector< uhal::ValWord<uint32_t> > infos;
    BOOST_FOREACH ( uint32_t r, regions ) {
        this->getNode("csr.ctrl.quad_sel").write(r);
        infos.push_back( this->getNode("id.region_info").read() );
    }
    
    // Execute all reads in one go
    this->getClient().dispatch();
    
    // Identify regions with buffers
    vector<uint32_t> bufregs;
    for ( size_t i(0); i<regions.size(); ++i ) {
        if ( infos[i].value() != RegBuffer && infos[i].value() != RegMGT ) continue;
        bufregs.push_back( regions[i] );
    }

    return bufregs;

}

std::vector<uint32_t>
CtrlNode::getMGTRegions() const {
    
    vector<uint32_t> regions = this->getRegions();
    
    // Queue region info read requests
    vector< uhal::ValWord<uint32_t> > infos;
    BOOST_FOREACH ( uint32_t r, regions ) {
        this->getNode("csr.ctrl.quad_sel").write(r);
        infos.push_back( this->getNode("id.region_info").read() );
    }
    
    // Execute all reads in one go
    this->getClient().dispatch();
    
    // Identify regions with buffers.
    vector<uint32_t> mgtRegions;
    for ( size_t i(0); i<regions.size(); ++i ) {
        if ( infos[i].value() != RegMGT ) continue;
        mgtRegions.push_back( regions[i] );
    }

    return mgtRegions;
}

boost::unordered_map<std::string, uint32_t>
CtrlNode::getGenerics() const {
    return snapshot( this->getNode("id.generics") );
}

CtrlNode::Clock40Guard::Clock40Guard(const CtrlNode& aCtrl, double aMilliSleep) :
    mCtrl(aCtrl), mMilliSleep(aMilliSleep) {
    cout << "Clock40Sentry constructor: resetting clock 40" << endl;
    mCtrl.resetClock40(0x1);
}

CtrlNode::Clock40Guard::~Clock40Guard() {
    cout << "Clock40Sentry destructor: releasing clock 40" << endl;
    log( Debug(), "Clock40Sentry destructor: releasing clock 40");
    mCtrl.resetClock40(0x0);
    millisleep(mMilliSleep);
    mCtrl.waitClk40Lock();    
}

}

