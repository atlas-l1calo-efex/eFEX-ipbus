/* 
 * File:   SI570Node.cpp
 * Author: ale
 * 
 * Created on June 2, 2014, 1:32 PM
 */

#include "mp7/SI570Node.hpp"

// MP7 Headers
#include "mp7/exception.hpp"

namespace mp7 {
UHAL_REGISTER_DERIVED_NODE(SI570Node);

SI570Node::SI570Node(const uhal::Node& aNode) :
OpenCoresI2C(aNode) {
}

SI570Node::SI570Node(const uhal::Node& aNode, uint8_t aAddr) :
OpenCoresI2C(aNode, aAddr) {
}

SI570Node::~SI570Node() {
}

void
SI570Node::configure(const std::string& aFilename) const {
    using namespace uhal;
    std::string lLine;
    std::ifstream lFile(aFilename.c_str());

    if (!lFile.is_open()) {
        exception::MP7HelperException lExc;
        log(lExc, aFilename, " was not found!");
        log(lExc, "Throwing at ", ThisLocation());
        throw lExc;
    }

    while (lFile.good()) {
        std::getline(lFile, lLine);

        if (lLine[0] == '#') {
            continue;
        }

        if (lLine.length() == 0) {
            break;
        }

        std::stringstream lStr;
        uint32_t lAddr(0), lData(0);
        char lDummy1, lDummy2;
        lStr << lLine;
        lStr >> std::dec >> lAddr >> lDummy1 >> std::hex >> lData >> lDummy2;
        //    log( Info() ,  "Register Address = ", Integer( lAddr ), " : Register Value = " , Integer(  lData, IntFmt<uhal::hex>() ) );
        log(Info(), "Register Address = ", Integer(lAddr), " : Register Value = ", Integer(lData));
        this->writeI2C(lAddr, lData);
    }

    lFile.close();

}

}

namespace mp7 {

//___________________________________________________________________________//
SI570Slave::SI570Slave(const opencores::I2CBaseNode* aMaster, uint8_t aSlaveAddress) : opencores::I2CSlave(aMaster,aSlaveAddress) {
}


SI570Slave::~SI570Slave() {
}


void
SI570Slave::configure(const std::string& aFilename) const {
    using namespace uhal;
    std::string lLine;
    std::ifstream lFile(aFilename.c_str());

    if (!lFile.is_open()) {
        exception::MP7HelperException lExc;
        log(lExc, aFilename, " was not found!");
        log(lExc, "Throwing at ", ThisLocation());
        throw lExc;
    }

    while (lFile.good()) {
        std::getline(lFile, lLine);

        if (lLine[0] == '#') {
            continue;
        }

        if (lLine.length() == 0) {
            break;
        }

        std::stringstream lStr;
        uint32_t lAddr(0), lData(0);
        char lDummy1, lDummy2;
        lStr << lLine;
        lStr >> std::dec >> lAddr >> lDummy1 >> std::hex >> lData >> lDummy2;
        //    log( Info() ,  "Register Address = ", Integer( lAddr ), " : Register Value = " , Integer(  lData, IntFmt<uhal::hex>() ) );
        log(Info(), "Register Address = ", Integer(lAddr), " : Register Value = ", Integer(lData));
        this->writeI2C(lAddr, lData);
    }

    lFile.close();

}

//___________________________________________________________________________//
UHAL_REGISTER_DERIVED_NODE(SI570Node2g);


SI570Node2g::SI570Node2g( const uhal::Node& aNode ) : opencores::I2CBaseNode(aNode), SI570Slave(this, this->getSlaveAddress("i2caddr") ) {
}


SI570Node2g::SI570Node2g( const SI570Node2g& aOther ) : opencores::I2CBaseNode(aOther), SI570Slave(this, this->getSlaveAddress("i2caddr") ) {

}


SI570Node2g::~SI570Node2g() {
}

}
