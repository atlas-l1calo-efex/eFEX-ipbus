#include "mp7/DatapathNode.hpp"

// MP7 Headers
#include "mp7/Utilities.hpp"
#include "mp7/exception.hpp"

// uHal Headers
#include "uhal/log/log.hpp"
#include "mp7/LinkDRPManager.hpp"

// C++ Headers
#include <algorithm>

// Boost headers
#include <boost/lexical_cast.hpp>

// Namespace resolution
using namespace std;
using namespace uhal;

/*

 * Notes
 * =====
 * 
 * Selectable from the main ctrl register
 *  * quad
 *  * channel
 *  * rx/tx buffer
 * 
 *  datapath.csr registers
 *    mode: defines the behavior of the buffer
 *      0 = latency buffer, 1 = capture, 2 = playback, 3 = repeating playback
 *    datasrc: controls the data switch (where the data are coming from)
 *      0 = normal data, 1 = playback, 2 = pattern, 3 = zeroes
 *    daq_bank: what daq bank 
 *    trig_bx: This is the BX number *before* you want to start capturing / playing 
 *      data. i.e. to send from bx0 onwards, set this to 0xdeb (i.e. one less than
 *      the bunch length).
 *    max_word: This is the last location in the buffer to use, i.e. to send 16 words,
 *       set this to 0xf. Note that this is in 240MHz samples, not BX - multiply 
 *       BX by six as the TMT is currently configured.
 * 
 * cap  registers: Rx buffer
 * play registers: Tx buffer
 *
 * Capture range
 * buffers.csr.cap_bx.bx_high c00
 * buffers.csr.cap_bx.bx_low 0
 *
 * Playback range
 * buffers.csr.play_bx.bx_high c00
 * buffers.csr.play_bx.bx_low 0
 *
 * TX and RX buffer modes
 *  buffer modes (both rx and tx):
 *  0 disabled
 *  1 capture
 *  2 playback
 *  3 pattern
 * buffers.csr.mode.tx_buf_mode 3
 * buffers.csr.mode.rx_buf_mode 3
 *
 * Capture/Playback range (Rx/Tx) switches
 * buffers.csr.mode.cap_bx_en 0
 * buffers.csr.mode.play_bx_en 1
 *
 * Loopbacks:
 * External
 * buffers.csr.mode.loopback_en 0

 * Internal
 * buffers.csr.mode.rx_datasrc 0
 */

namespace mp7 {
UHAL_REGISTER_DERIVED_NODE(DatapathNode);

// PUBLIC METHODS

DatapathNode::DatapathNode(const uhal::Node& aNode) :
uhal::Node(aNode),
//mBufferSize(aNode.getNode("buffers.data").getSize() / 2),
//mPathManager(0x0),
        mDRPManager(0x0) {
//    mPathManager = new PathManager(this->getNode("csr"), this->getNode("buffers"));
    mDRPManager = new LinkDRPManager(this->getNode("region.drp"), this->getNode("region.drp_com"));
}

DatapathNode::DatapathNode(const DatapathNode& other) :
uhal::Node(other),
//mBufferSize(other.getNode("buffers.data").getSize() / 2),
//mPathManager(0x0),
        mDRPManager(0x0) {
//    mPathManager = new PathManager(this->getNode("csr"), this->getNode("buffers"));
    mDRPManager = new LinkDRPManager(this->getNode("region.drp"), this->getNode("region.drp_com"));
}

DatapathNode::~DatapathNode() {
    //      std::cout << this << " ~DatapathNode A" << std::endl;
//    delete mPathManager;
    delete mDRPManager;
    //      std::cout << this << " ~DatapathNode B" << std::endl;

}

const LinkDRPManager& DatapathNode::drpManager() const {
    return *mDRPManager;
}
}
