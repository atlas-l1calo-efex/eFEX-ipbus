#include "mp7/TTCNode.hpp"

// MP7 Headers
#include "mp7/Utilities.hpp"
#include "mp7/exception.hpp"

// uHAL Headers
#include "uhal/log/log.hpp"

// Namespace resolution
using namespace std;
using namespace uhal;

namespace mp7 {
UHAL_REGISTER_DERIVED_NODE(TTCNode);

// PRIVATE CONSTANTS
const uint32_t TTCNode::mBTestCode = 0xc;

// PUBLIC METHODS

TTCNode::TTCNode(const uhal::Node& aNode) :
uhal::Node(aNode) {
}

TTCNode::~TTCNode() {
}

void
TTCNode::enable(bool enable) const {
    getNode("csr.ctrl.ttc_enable").write(enable);
    getClient().dispatch();
}

void
TTCNode::generateInternalBC0(bool enable) const {
    getNode("csr.ctrl.int_bc0_enable").write(enable);
    getClient().dispatch();
}

void
TTCNode::clear() const {
    getNode("csr.ctrl.err_ctr_clear").write(1);
    getNode("csr.ctrl.err_ctr_clear").write(0);
    getNode("csr.ctrl.ctr_clear").write(1);
    getNode("csr.ctrl.ctr_clear").write(0);
    getClient().dispatch();
}

void
TTCNode::sendBGo(uint32_t aCode) const {
    getNode("csr.ctrl.b_send").write(aCode);
    getClient().dispatch();
}

void
TTCNode::sendBTest() const {
    this->sendBGo(mBTestCode);
}

void
TTCNode::captureData(double wait) const {
    log(Notice(), "Capturing (", wait, " sec)");
    getNode("csr.ctrl.buf_go").write(1);
    getClient().dispatch();
    // wait a while. capture is only triggered at begin of orbit
    millisleep(wait * 1000);
    getNode("csr.ctrl.buf_go").write(0);
    getClient().dispatch();
    log(Notice(), "Capture completed");
}

std::vector<uint64_t>
TTCNode::captureBGOs(bool aMaskBC0, double aWaitSec) const {
    log(Notice(), "Capturing BGOs (", aWaitSec, " sec)");
    getNode("csr.ctrl.mask_hist_bc0").write(aMaskBC0);
    cout << "Mask BC0: " << aMaskBC0 << endl;

    // Resets the fifo and begins capturing bgos
    getNode("csr.ctrl.fill_hist_buf").write(0x1);
    getClient().dispatch();

    // Wait while collecting data
    millisleep(aWaitSec * 1000);

    // Stop capturing
    getNode("csr.ctrl.fill_hist_buf").write(0x0);
    getClient().dispatch();
   
    size_t fifoMax(0x400);
    std::vector<uint64_t> history;history.reserve(fifoMax);

    uhal::ValWord<uint32_t> info, orbit_ctr;
    for( uint32_t i(0x0); i<fifoMax; ++i ) {

        // Read info first
        info = getNode("hist.info").read();
        getClient().dispatch();

        // Check if valid (otherwise get out of here)
        bool valid = (info >> 31) & 1;
        cout << i << " - " << valid << " : 0x" << std::hex << (uint32_t)info << endl;
        if ( not valid ) break;

        // Read info first
        orbit_ctr = getNode("hist.orbit_ctr").read();
        getClient().dispatch();

        uint64_t x = ((uint64_t) orbit_ctr.value() << 32) + (uint64_t) info.value();
        history.push_back(x);

    }
    
    return history;
}

std::vector<uint64_t>
TTCNode::captureBGOs_old(bool aMaskBC0, double aWaitSec) const {
    // Resets the TTC history buffer and dumps the TTC commands collected over hte last X seconds
    log(Notice(), "Capturing BGOs (", aWaitSec, " sec)");
    getNode("csr.ctrl.mask_hist_bc0").write(aMaskBC0);
    // flush the fifo
    getNode("hist").readBlock(0x1000);
    // trigger capture
    getNode("csr.ctrl.fill_hist_buf").write(0x1);
    getNode("csr.ctrl.fill_hist_buf").write(0x0);
    getClient().dispatch();
    millisleep(aWaitSec * 1000);
    log(Notice(), "Capture completed");
    uhal::ValVector<uint32_t> fifoBlock = getNode("hist").readBlock(0x2000);
    getClient().dispatch();
    size_t depth = 0x2000;
    vector<uint64_t> history;
    history.reserve(depth);

    // Each entry is made of 2 words
    for (uint32_t i(0); i < fifoBlock.size(); i += 2) {
        uint64_t h = ((uint64_t) fifoBlock[i + 1] << 32) + (uint64_t) fifoBlock[i];
        // Decoded values
        bool valid = (h >> 31) & 1;

        if (!valid) {
            break;
        }

        history.push_back(h);
    }

    return history;

    // for (uint32_t i(0); i < depth; ++i) {
    //     ValWord<uint32_t> v1 = getNode("hist").read();
    //     ValWord<uint32_t> v2 = getNode("hist").read();
    //     getClient().dispatch();
    //     uint64_t h = ((uint64_t) v2 << 32) + v1;
    //     // Decoded values
    //     bool valid = (h >> 31) & 1;
    //     //      bool isL1A = (h >> 24) & 1;
    //     //      uint32_t orbit = (h >> 32) & 0xffffff;
    //     //      uint32_t bx = (h >> 8) & 0xfff;
    //     //      uint32_t cmd = (h & 0xff);
    //     //    ostringstream oss;
    //     //    oss << setw(4) << i << '|'
    //     //      << " V: "   << (int) valid
    //     //      << " L1A: " << (int) isL1A
    //     //      << " orb: " << uintHexStr(orbit, 6)
    //     //      << " bx: "  << uintHexStr(bx, 3)
    //     //      << " cmd: " << uintHexStr(cmd, 2);

    //     if (!valid) {
    //         break;
    //     }

    //     //      history.push_back(
    //     //              strprintf("%4d | V:%1d L1A:%1d orb:0x%06x bx:0x%03x cmd:%02x", i, valid, isL1A, orbit, bx, cmd)
    //     //              );
    //     history.push_back(h);
    // }

    // return history;
}

void
TTCNode::waitBC0Lock() const {
    uhal::ValWord< uint32_t > bc0_lock(0);
    int countdown = 100;

    while (countdown > 0) {
        bc0_lock = this->getNode("csr.stat0.bc0_lock").read();
        this->getClient().dispatch();

        if (bc0_lock) {
            break;
        }

        //countdown = -100;
        //else
        //countdown -= 1;
        countdown--;
        millisleep(10);
    }

    if (countdown == 0) {
        exception::BC0LockFailed lExc;
        log(lExc, "Timed out waiting for bc0_lock signal");
        throw lExc;
    }

    log(Notice(), "TTC BC0 Locked");
}

double
TTCNode::measureClockFreq(FreqClockChannel aChan, double aWait) const {

    getNode("freq.chan_sel").write(aChan);
    getClient().dispatch();
    millisleep(aWait * 1000);
    uhal::ValWord<uint32_t> fq = getNode("freq.freq.count").read();
    uhal::ValWord<uint32_t> fv = getNode("freq.freq.valid").read();
    getClient().dispatch();

    if (fv == 0) {
        throw runtime_error("TTC Frequency Measurement not valid");
    }

    return int ( fq) * 119.20928 / 1000000;
}

double
TTCNode::freqClk40(double aWait) const {

    getNode("freq.chan_sel").write(Clock40);
    getClient().dispatch();
    millisleep(aWait * 1000);
    uhal::ValWord<uint32_t> fq = getNode("freq.freq.count").read();
    uhal::ValWord<uint32_t> fv = getNode("freq.freq.valid").read();
    getClient().dispatch();

    if (fv == 0) {
        exception::TTCFrequencyInvalid lExc;
        log(lExc, "Failed measuring TTC clock frequency");
        throw lExc;
    }

    return int ( fq) * 119.20928 / 1000000;
}

boost::unordered_map<std::string, std::string>
TTCNode::report() const {
    boost::unordered_map<std::string, std::string> report;
    boost::unordered_map<std::string, uint32_t> ttccsr = snapshot(getNode("csr"));

    double freq = -1.;
    try {
        freq = this->freqClk40(1.1);
    } catch (const exception::TTCFrequencyInvalid e) {
        // Nothing to do. The exception is already logged when thrown
    }
    
    report["Measured Clock40 Frequency"] = ( freq >= 0. ? strprintf("%.3f MHz", freq) : "Failed");
    report["BC0 Register"] = strprintf("0x%x", ttccsr["stat0"] >> 16);
    report["BC0 Internal"] = ( ttccsr["ctrl.int_bc0_enable"] ? "Ok" : "Off");
    report["BC0 Lock"]     = ( ttccsr["stat0.bc0_lock"] ? "Yes" : "No");
    report["BC0 Error"]    = ( ttccsr["stat0.bc0_err"] ? "Yes" : "No");
    report["Dist Lock"]    = ( ttccsr["stat0.dist_lock"] ? "Yes" : "No");
    
    return report;

}

}

