/* 
 * File:   MP7Controller.cpp
 * Author: ale
 * 
 * Created on July 17, 2014, 9:23 PM
 */

#include "mp7/MP7Controller.hpp"

// uHAL Headers
#include "uhal/log/log.hpp"

// MP7 Headers
#include "mp7/CtrlNode.hpp"
#include "mp7/ClockingNode.hpp"
#include "mp7/TTCNode.hpp"
#include "mp7/GTHQuadNode.hpp"
#include "mp7/ChanBufferNode.hpp"
#include "mp7/DatapathNode.hpp"
#include "mp7/AlignmentNode.hpp"
#include "mp7/MmcPipeInterface.hpp"
#include "mp7/exception.hpp"
#include "mp7/Utilities.hpp"

// Boost Headers
#include <boost/thread/mutex.hpp>
#include <boost/assign/list_inserter.hpp> // for 'insert()'


// Namespace resolution
using namespace std;
using namespace uhal;


namespace mp7 {

MP7Controller::MP7Controller(uhal::HwInterface aInterface) :
HwInterface(aInterface), mCtrl(0x0), mClocking(0x0), mTTC(0x0), mDatapath(0x0), mAlign(0x0), mMGT(0x0), mChanBuffer(0x0), mMmcPipe(0x0) {
    this->Constructor();
}

MP7Controller::~MP7Controller() {
}

void
MP7Controller::Constructor() {

    // Extract MP7 specific nodes
    mCtrl = &(this->getNode<CtrlNode>("ctrl"));
    mClocking = &(mCtrl->getNode<ClockingNode>("clocking"));

    mTTC = &(this->getNode<TTCNode>("ttc"));

    mDatapath = &(this->getNode<DatapathNode>("datapath"));
    mAlign = &(mDatapath->getNode< AlignmentNode>("align"));
    mMGT = &(mDatapath->getNode< GTHQuadNode>("region.mgt"));
    mChanBuffer = &(mDatapath->getNode< ChanBufferNode>("region.buffer"));
    mMmcPipe = &(this->getNode< MmcPipeInterface>("uc"));

    // read and store generics
    mGenerics = mCtrl->getGenerics();
    mRegions = mCtrl->getRegions();
    mBufferRegions = mCtrl->getBufferRegions();
    mMGTRegions = mCtrl->getMGTRegions();


    //    mChannels = vector<uint32_t>(boost::counting_iterator<uint32_t>(0), boost::counting_iterator<uint32_t>( 4*mGenerics["nquad"] ) );

    //    mActiveInputs = mChannels;
    //    mActiveOutputs = mChannels;


    log( Debug(), "MP7 Controller constructed");

}

void
MP7Controller::reset() {
    log(Debug(), "Locking for reset");
    {
        // Acquire exclusive access to the board
        boost::lock_guard<boost::mutex> lLock(mMutex);

        log(Debug(), "Reset & Clock");

        this->configureClocking(false, "internal");
//        this->configureClocking(true, "external");
        
        log(Debug(), "Enabling TTC");
//        this->configureTTC( true, false);
        this->configureTTC( false, true);
        
        mTTC->report();
        
    }
    log(Debug(), "Unlocking");

    
}

void
MP7Controller::configureTTC(bool aEnable, bool aBC0Internal) {

    //        self.log.info('Configuring ttc')

    // Enable ttc L1As and Bgos commands
    mTTC->enable(aEnable);

    // Clear counters
    mTTC->clear();

    mTTC->generateInternalBC0(aBC0Internal);

    // Wait for the BC0 lock only for external input
    if (aEnable) mTTC->waitBC0Lock();
}

void
MP7Controller::configureClocking( bool aExternalClk40, const std::string& aClkCfg) {
    /**
     * Clocking configuration needs
     * - Clock40: External/Internal
     * - SI configuration:
     *  R1: si5326 chip configuration
     *  XE: si5236_top, si5326_bot and si570 config
     */

    {
        // Use a guard to keep the clock in reset
        CtrlNode::Clock40Guard guard(*mCtrl);

        log(Notice(), "Selecting clk40 source ", (aExternalClk40 ? "external" : "internal" ));
        mCtrl->selectClk40Source( aExternalClk40 );
        sleep(1);
        
        mClocking->configure(aClkCfg);
        
    } // Clock40Guard deleted
    
}

boost::unordered_map<std::string, std::string>
MP7Controller::refClkReport() {
    boost::unordered_map<std::string, std::string> report;

    BOOST_FOREACH( uint32_t l, mMGTChannels ) {
        string tag = strprintf("[%02d]",l);
        double refclock = mTTC->measureClockFreq(TTCNode::RefClock, 1.1);
        double rxclock  = mTTC->measureClockFreq(TTCNode::RxClock, 1.1);
        double txclock  = mTTC->measureClockFreq(TTCNode::TxClock, 1.1);
        boost::assign::insert(report)
                ( "RefClk"+tag, strprintf("%.3f Mhz", refclock) )
                ( "RxClk"+tag , strprintf("%.3f Mhz", rxclock) );
                ( "TxClk"+tag , strprintf("%.3f Mhz", txclock) );
    }
    return report;
}

} // namespace mp7

