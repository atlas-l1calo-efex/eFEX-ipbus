#include "mp7/ClockingNode.hpp"

// C++ Headers
#include <stdexcept>

// Boost Headers
#include <boost/unordered_map.hpp>
#include <boost/assign.hpp>

// MP7 Headers
#include "mp7/Utilities.hpp"
#include "mp7/exception.hpp"

// uHal headers
#include "uhal/ValMem.hpp"
#include "mp7/SI5326Node.hpp"

// Namespace resolution
using namespace std;
using namespace uhal;

namespace mp7 {

// DerivedNode registration
UHAL_REGISTER_DERIVED_NODE(ClockingNode);

ClockingNode::ConfigMap ClockingNode::makeDefaultConfigs() {
    ConfigMap configs;
    Config internal = {
        Disconnected,
        Oscillator,
        ""
    };
    Config external = {
        ExternalAMC13,
        Oscillator,
        "${MP7_TESTS}/etc/mp7/sicfg/si5326/MP7_SI5326_20130606_40.1MHz_CKIN1_to_160.4MHz_CKOUT1_NoIncDec_Regs.txt"
    };

    configs["internal"] = internal;
    configs["external"] = external;

    return configs;
}

const boost::unordered_map<std::string, ClockingNode::Config> ClockingNode::mConfigurations = makeDefaultConfigs();

ClockingNode::ClockingNode(const uhal::Node& node) :
uhal::Node(node) {
}

ClockingNode::~ClockingNode() {
}

void
ClockingNode::configure(const std::string aConfig) const {
    
    ConfigMap::const_iterator it = mConfigurations.find(aConfig);
    if (it == mConfigurations.end()) {
        exception::MP7HelperException lExc;
        log(lExc, "Configuration ", aConfig, " could not be found");
        throw lExc;
    }
    
    const SI5326Node& si5326 = getNode<SI5326Node>("i2c_si5326");

    const Config& clkcfg = it->second;

    this->configureXpoint(clkcfg.clk40, clkcfg.refClk);

    if (!clkcfg.si5626Cfg.empty()) {

        // Reset the SI chip
        this->si5326Reset();

        // Take a nap
        millisleep(1000);

        // And then reconfigure
        si5326.configure(shellExpandPath(clkcfg.si5626Cfg));

        // Take a nap for 1/2 a second
        millisleep(500);

        // Wait the si5326 to recover
        this->si5326WaitConfigured();

        si5326.debug();
    }
}

void
ClockingNode::configureXpoint(Clk40Select aClk40Src, RefClkSelect aRefSrc) const {

    switch (aClk40Src) {
        case ExternalAMC13:
            // Clock 40 routing from AMC13
            this->configureU36(3, 3, 0, 0);
            break;
        case ExternalMCH:
            // Clock 40 routing from MCH, external clock generator
            this->configureU36(1, 1, 0, 0);
            break;
        case Disconnected:
            // From internal oscillator
            // U36 don't matter. Best to have a default anyway?
            // 0,0,0,0 is the poweron value
            this->configureU36(1, 1, 0, 0);
            break;
        default:
            throw runtime_error("Invalid clock 40 source");
    }

    switch (aRefSrc) {
        case Oscillator:
            // Refclock from the internal oscillator
            this->configureU3(1, 1, 1, 1);
            this->configureU15(1, 1, 1, 1);
            break;
        case ClockCleaner:
            // Refclock from clock chip
            this->configureU3(3, 3, 3, 3);
            this->configureU15(3, 3, 3, 3);
            break;
        default:
            throw runtime_error("Invalid reference clock source");
            break;
    }
}

void
ClockingNode::configureUX(const std::string& aChip, uint SelForOut0, uint SelForOut1, uint SelForOut2, uint SelForOut3) const {

    const Node& ctrl = this->getNode("csr.ctrl");
    ctrl.getNode("selforout1_" + aChip).write(SelForOut0);
    ctrl.getNode("selforout2_" + aChip).write(SelForOut1);
    ctrl.getNode("selforout3_" + aChip).write(SelForOut2);
    ctrl.getNode("selforout4_" + aChip).write(SelForOut3);

    ctrl.getNode("prog_" + aChip).write(1);
    this->getClient().dispatch();
    usleep(10);
    ctrl.getNode("prog_" + aChip).write(0);
    this->getClient().dispatch();
    // wait for the cross point switch to assert its done signal
    const string stat_done = "csr.stat.done_" + aChip;
    ValWord< uint32_t > done(0);
    int countdown = 100;

    while (countdown > 0) {
        done = this->getNode(stat_done).read();
        this->getClient().dispatch();

        if (done) {
            break;
        }

        --countdown;
    }

    if (countdown == 0) {
        exception::XpointConfigTimeout lExc;
        log(lExc, "Timed out while waiting for Xpoint ", aChip, " to complete configuration (100 tries)");
        throw lExc;
    }
}

void
ClockingNode::configureU3(uint SelForOut0, uint SelForOut1, uint SelForOut2, uint SelForOut3) const {
    // Inputs to xpoint_u3          	 Outputs to xpoint_u3
    // ===================          	 ===================
    // Input 0 = osc2               	 Output 0 = refclk0
    // Input 1 = osc1               	 Output 1 = refclk1
    // Input 2 = clk2               	 Output 2 = refclk2
    // Input 3 = clk1               	 Output 3 = refclk3
    this->configureUX("u3", SelForOut0, SelForOut1, SelForOut2, SelForOut3);
}

void
ClockingNode::configureU15(uint SelForOut0, uint SelForOut1, uint SelForOut2, uint SelForOut3) const {
    // Inputs to xpoint_u15            Outputs to xpoint_u15
    // ===================             ===================
    // Input 0 = osc2                  Output 0 = refclk4
    // Input 1 = osc1                  Output 1 = refclk5
    // Input 2 = clk2                  Output 2 = refclk6
    // Input 3 = clk1                  Output 3 = refclk7
    this->configureUX("u15", SelForOut0, SelForOut1, SelForOut2, SelForOut3);
}

void
ClockingNode::configureU36(uint SelForOut0, uint SelForOut1, uint SelForOut2, uint SelForOut3) const {
    // Inputs to xpoint_u36           	 Outputs to xpoint_u36
    // Input 0 = si5326 clk1 output   	 Output 0 = si5326 clk1 input
    // Input 1 = TCLK-C               	 Output 1 = clk3
    // Input 2 = TCLK-A               	 Output 2 = clk2
    // Input 3 = FCLK-A               	 Output 3 = clk1
    // ---------------------------------------------------------------------
    // Wish to send MCH TCLK-A (input-2) to si5326 (output-0) and for all
    // other outputs to be driven by si5326 (input-0)
    // ---------------------------------------------------------------------
    this->configureUX("u36", SelForOut0, SelForOut1, SelForOut2, SelForOut3);
}

void
ClockingNode::si5326Reset() const {
    // Reset the si5326
    this->getNode("csr.ctrl.rst_si5326").write(0);
    this->getClient().dispatch();
    // minimum reset pulse width is 1 microsecond, we go for 5
    usleep(5);
    this->getNode("csr.ctrl.rst_si5326").write(1);
    this->getClient().dispatch();
}

void
ClockingNode::si5326WaitConfigured(uint32_t aMaxTries) const {
    uint32_t countdown(aMaxTries);
    while (countdown > 0) {
        ValWord<uint32_t> si5326_lol = this->getNode("csr.stat.si5326_lol").read();
        ValWord<uint32_t> si5326_int = this->getNode("csr.stat.si5326_int").read();
        this->getClient().dispatch();

        if (si5326_lol.value() == 0 && si5326_int.value() == 0) {
            break;
        }

        millisleep(1);
        --countdown;
    }

    if (countdown == 0) {
        log(Error(), "timed out waiting for si5326 to recover from configuration");
        exception::SI5326ConfigurationTimeout lExc;
        log(lExc, "Timed out while waiting for SI5326 to complete configuration (", uhal::Integer(aMaxTries), " ms)");
        throw lExc;
    } else {
        log(Notice(), "SI5326 finished configuring after ", uhal::Integer(aMaxTries - countdown), " ms");
    }
}

bool
ClockingNode::si5326LossOfLock() const {
    ValWord<uint32_t> si5326_lol = this->getNode("csr.stat.si5326_lol").read();
    this->getClient().dispatch();
    return ( bool) si5326_lol;
}

bool
ClockingNode::si5326Interrupt() const {
    ValWord<uint32_t> si5326_int = this->getNode("csr.stat.si5326_int").read();
    this->getClient().dispatch();
    return ( bool) si5326_int;
}

}

