/* 
 * File:   LinkDRPManager.cpp
 * Author: ale
 * 
 * Created on June 3, 2014, 2:03 PM
 */

#include "mp7/LinkDRPManager.hpp"

LinkDRPManager::LinkDRPManager( const uhal::Node& aDRPChanNode, const uhal::Node& aDRPCommonNode ) :
  mChannel(aDRPChanNode), mCommon(aDRPCommonNode) {
}

LinkDRPManager::~LinkDRPManager() {
//  std::cout << "LinkDRPManager::Destructor" << std::endl; 
}

