#!/bin/env python

import controller
import argparse
import cli_utils
                
  
class ButlerPlugin:
  """
  Base class for butler plugins
  """
  _dftstr=' (default: \'%(default)s\')'
  
  def __init__(self):
    """Documentation"""
    pass
  
  def addParser(self,parser):
    """Documentation"""
    pass
  
  def __call__(self, **kwargs):
    """Documentation"""
    pass
    
class BoardPlugin(ButlerPlugin):
  """
  Documentation
  """
  
  def addCommonArguments(self,parser):
    """Documentation"""
    subp.add_argument('board',help='Board to connect to')

    # subp.add_argument('--class', dest='classname',choices=controller.classnames, default='MP7Controller', help='MP7Controller class'+self._dftstr)  
    subp.add_argument('-m','--model', dest='model',choices=mp7ctrl.models.keys(), default='r1', help='MP7 model class'+dftstr)    
    subp.add_argument('--enablelinks', action=cli_utils.IntListAction, default=None, help='Links to control'+self._dftstr)


if __name__ == "__main__":
  import argparse
  
  connectionfiles = [
    'file://${MP7_TESTS}/etc/mp7/uhal/connections-test.xml',
    'file://${MP7_TESTS}/etc/mp7/uhal/connections-904.xml',
    'file://${MP7_TESTS}/etc/mp7/uhal/connections-july14.xml',
  ]

  dftstr=' (default: \'%(default)s\')'
  connectionstring = ';'.join(connectionfiles);
        
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

  # Core uhal parameters
  parser.add_argument('-c','--connections', default=connectionstring, help='Uhal connection file'+dftstr)
  parser.add_argument('-v', '--verbose', action='count', default=0)
  parser.add_argument('--log',default=None, help='Log file'+dftstr)
  parser.add_argument('--timeout',type=int,default=1000, help='IPBus timeout'+_dftstr)  

  # Utility commands, based on uhal only
  subparsers = parser.add_subparsers(dest = 'cmd')
  subp = subparsers.add_parser('show', help='Show the list of known devices')

  subp = subparsers.add_parser('connect', help='Test the connection to the selected board')
  subp.add_argument('board',help='Board to connect to')

  subp = subparsers.add_parser('ipython', help='Create an MP7Tester for python interactive shell')
  subp.add_argument('board',help='Board to connect to')
  subp.add_argument('--class', dest='classname',choices=controller.classnames, default='MP7Controller', help='MP7Controller class'+self._dftstr)  
  
  
#  addCommonArguments(subp)
  # show
  # connect
  # ipython
  # do
  #   --> board reset
  #   --> board mgts
  print parser.parse_args()
  
  
