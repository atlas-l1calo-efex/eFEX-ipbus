/*
 * File:   testhex.cxx
 * Author: ale
 *
 * Created on November 20, 2013, 10:47 PM
 */

#include <cstdlib>
#include "mp7/Parameters.hpp"
#include "mp7/Utilities.hpp"

using namespace std;

/*
 *
 */
int main ( int argc, char** argv )
{

    mp7::Parameters pars;
    pars.set("stocazz", std::string("stacepp"));
    pars["staminchia"] = 3;
    
    cout << "stocazz: " << pars.get<std::string>("stocazz") << endl;
    cout << "staminchia: " << pars.get<int>("staminchia") << endl;

    std::string path = "${MP7_TESTS}";
    cout << "expand " <<  path << " -> " << mp7::shellExpandPath(path) << endl;
    
}

