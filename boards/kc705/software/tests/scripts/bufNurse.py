#!/bin/env python


import argparse
import logging
import mp7tools.helpers as hlp
import mp7tools.buffers as buffers
import mp7tools.dataio as dataio


class IntListAction(argparse.Action):
    def __init__(self, *args, **kwargs):
        super(IntListAction, self).__init__(*args, **kwargs)
        # self._var  = var
        # self._sep  = sep
        # self._dash = dash
        self._sep  = ','
        self._dash = '-'

    def __call__(self, parser, namespace, values, option_string=None):

        numbers=[]
        items = values.split(self._sep)
        for item in items:
            nums = item.split(self._dash)
            if len(nums) == 1:
                # single number
                numbers.append(int(item))
            elif len(nums) == 2:
                i = int(nums[0])
                j = int(nums[1])
                if i > j:
                    parser.error('Invalid interval '+item)
                numbers.extend(range(i,j+1))
            else:
               parser.error('Malformed option (comma separated list expected): %s' % values)

        setattr(namespace, self.dest, numbers)

class BufferURIAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):

        if not values.startswith('file://'):
            values = 'file://'+values

        from mp7tools.buffers import BoardDataSource
        try:
            uri = BoardDataSource.validateDataUri(values)
        except RuntimeError as re:
            parser.error(str(re))

        setattr(namespace, self.dest, uri) 

def parseArgs():

    dftstr=' (default: \'%(default)s\')'

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('source',action=BufferURIAction)
    parser.add_argument('dest')
    parser.add_argument('--replacechans', action=IntListAction, default=[], help='Links to control'+dftstr)
    parser.add_argument('--master', default=0, help='Master link'+dftstr)
    parser.add_argument('--offset', default=0, type=int, help='Datavalid offset'+dftstr)
    args = parser.parse_args()
    return args


# logging initialization
hlp.initLogging( logging.DEBUG )

args = parseArgs()



dataSource = buffers.BoardDataSource(args.source)
indata = dataSource.data

master = indata.getlinkbuffer(args.master)
valids = [ (k>>32) for k in master ]
fakedata = [ (k<<32) for k in valids ]

if args.offset == 0:
    pass
elif args.offset < 0:
    fakedata = fakedata[-args.offset:] + [0]*-args.offset
elif args.offset > 0:
    fakedata = [0]*args.offset + fakedata[:-args.offset]

# print 'len',len(valids),len(fakedata)

# for v,f in zip(valids,fakedata):
#     print v,hex(f)

# for i,v in enumerate(valids):
#     print i,v

outdata = dataio.BoardData(dataSource.data.id)

for l in indata.links:

    if not l in args.replacechans:
        outdata.addlink(l,indata.getlinkbuffer(l))
        continue

    outdata.addlink(l,fakedata)    



dataio.writeValid( outdata, args.dest )
