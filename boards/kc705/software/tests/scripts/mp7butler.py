#!/bin/env python
'''
--------------
MP7 Test Suite
--------------
Title: mp7butler
Author: Alessandro Thea

All purpose script to manage an mp7 board

Example of usage

mp7butler.py show
mp7butler.py reset MP7_DEVEL --clksrc=internal
mp7butler.py links MP7_DEVEL 


Brief: Buffer capture and data transmission example
Description:
This script provides an example to work with MGT buffers: upload and capture data.
As in the previous example the high level actions are from mp7tools.tester.MP7Tester.

Valid buffer modes:
    'algoPlay':
    'algoPatt'
    'loopPlay'
    'loopPatt'
    'captureRx'
    'captureTx'
    'captureRxTx'

Sketch of buffers interconnections
        -----     ----
    -<-| MGT |-<-| Tx |---<---
        -----     ----       |
          |                  ^
          v              ----------
          |             |          |
      MGT |             |   Algo   | 
     loop |             |   Block  | 
          |             |          |
          v              ----------
          |                  ^
        -----     ----       |
    ->-| MGT |->-| Rx |--->---
        -----     ----

'''


from __future__ import print_function

import os.path
import sys
import time

import argparse
import logging
from mp7tools.buffers import BoardDataSource
from mp7tools.buffers import Configurator3g as Configurator
import mp7tools.controller as mp7ctrl
import mp7tools.helpers as hlp
import uhal

class ButlerParser():
    _me = None

    @classmethod
    def get(cls):
        if not cls._me:
            cls._me = cls._createParser()

        return cls._me

    @staticmethod
    def _createParser():
        from mp7tools.cli_utils import convert, ExecVarAction, BxRangeTupleAction, IntListAction, BufferURIAction
    
        modes=Configurator.modes()
        clksrcs = [
            'internal',
            'external',
        ]
        clkOpts = [
            'si570',
            'si5326',
        ]
    
        dftstr=' (default: \'%(default)s\')'
        connectionfiles = [
          'file://${MP7_TESTS}/etc/mp7/connections-test.xml',
          'file://${MP7_TESTS}/etc/mp7/connections-904.xml',
          'file://${MP7_TESTS}/etc/mp7/connections-july14.xml',
        ]
        
        
        connectionstring = ';'.join(connectionfiles);
        
        def addCommonArguments(subp):
            subp.add_argument('board',help='Board to connect to')
            subp.add_argument('--timeout',type=int,default=1000, help='IPBus timeout'+dftstr)
#            subp.add_argument('--class', dest='classname',choices=mp7ctrl.classnames, default='MP7Controller', help='MP7Controller class'+dftstr)  
            subp.add_argument('-m','--model', dest='model',choices=mp7ctrl.models.keys(), default='r1', help='MP7 model class'+dftstr)  
            subp.add_argument('--enablelinks', action=IntListAction, default=None, help='Links to control'+dftstr)

        parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser.add_argument('-c','--connections', default=connectionstring, help='Uhal connection file'+dftstr)
        parser.add_argument('--log',default=None, help='Log file'+dftstr)
        parser.add_argument('-v', '--verbose', action='count', default=0)
        parser.add_argument('-q', '--quiet', action='store_true', default=False)

        subparsers = parser.add_subparsers(dest = 'cmd')
        subp = subparsers.add_parser('show', help='Show the list of known devices')

        subp = subparsers.add_parser('connect', help='Test the connection to the selected board')
        subp.add_argument('board',help='Board to connect to')
    
        subp = subparsers.add_parser('ipython', help='Create an MP7Tester for python interactive shell')
        addCommonArguments(subp)
    
        subp = subparsers.add_parser('exec', help='Execute a butler macro script')
        subp.add_argument('file',help='Macro file containing commands to execute')
        subp.add_argument('vars',help='Variables to be replaced in the macro file'+dftstr,nargs='*',action=ExecVarAction,)
        subp.add_argument('--repeat', default=1, type=int, help='Number of repetitions'+dftstr)
        subp.add_argument('--wait', default=0, type=int, help='Pause between checks'+dftstr)
        
        subp = subparsers.add_parser('reset', help='Reset the board and set the clocking')
        addCommonArguments(subp)
        subp.add_argument('--clksrc', choices=clksrcs, default='external', help='Clock source selection'+dftstr)
        subp.add_argument('--overrideBC0', choices=clksrcs, default=None, help='Clock source selection'+dftstr)
        subp.add_argument('--clkOpt', choices=clkOpts, dest='clkOpt',default='si570', help='MP7XE: Choose either si5326 or si570')
    
        subp = subparsers.add_parser('clocks', help='Measure the status of the clocks')
        addCommonArguments(subp)
    
        subp = subparsers.add_parser('mgts', help='Configure links')
        addCommonArguments(subp)
        subp.add_argument('--secondary', dest='secondary',  default=False, action='store_true',  help='Use secondary master'+dftstr) 
        subp.add_argument('--orbittag', dest='orbittag',  default=False, action='store_true',  help='Align to the orbit tag'+dftstr) 
        subp.add_argument('--loopback', dest='loopback',  default=False, action='store_true',  help='Activate MGT loopback'+dftstr)
        subp.add_argument('--forcepattern', dest='forcepattern',  default=False, action='store_true',  help='Force pattern generation in Tx buffers'+dftstr) 
        subp.add_argument('--fix-latency', dest='fixlatency', type=int, default=None, help='Aix the link latency to n frames'+dftstr)
        subp.add_argument('--no-config', dest='config',   default=True,  action='store_false', help='Configure links'+dftstr)
        subp.add_argument('--no-align', dest='align',     default=True,  action='store_false', help='Run link alignment'+dftstr)
        subp.add_argument('--no-check', dest='check',     default=True,  action='store_false', help='Check links for errors'+dftstr)

        subp = subparsers.add_parser('monitor', help='Monitor the board')
        addCommonArguments(subp)
        subp.add_argument('repeat', default=10, type=int, help='number of repetitions')
        subp.add_argument('--wait', default=5, type=int, help='pause between checks'+dftstr)
        subp.add_argument('--clear', dest='clear',  default=False, action='store_true',  help='Clear counters at the end'+dftstr)

        subp = subparsers.add_parser('buffers', help='Configure buffers')
        addCommonArguments(subp)
        subp.add_argument('mode',choices=modes)
        subp.add_argument('--inject', dest='data_uri', default='generate:empty', action=BufferURIAction, help='Source of data to be injected'+dftstr)
        subp.add_argument('--depth', dest='depth', default=0, type=int, help='Buffer depth'+dftstr) 
        subp.add_argument('--cap', dest='cap_bx',  default=(0x0,None), action=BxRangeTupleAction, help='Capture range '+dftstr)
        subp.add_argument('--play',dest='play_bx', default=(0x0,None), action=BxRangeTupleAction, help='Playback range'+dftstr)
        strip_group = subp.add_mutually_exclusive_group()
        strip_group.add_argument('--strip',     dest='strip', action='store_true', help='Strip header from incoming data'+dftstr)
        strip_group.add_argument('--no-strip',  dest='strip', action='store_false', help='Do not strip header from incoming data'+dftstr)
        insert_group = subp.add_mutually_exclusive_group()
        insert_group.add_argument('--insert', dest='insert', action='store_true',  help='Insert header on outgoing data'+dftstr)
        insert_group.add_argument('--no-insert', dest='insert', action='store_false',  help='Do not insert header on outgoing data'+dftstr)
        subp.set_defaults(strip=None, insert=None)

        subp = subparsers.add_parser('capture', help='Perform data capture')
        addCommonArguments(subp)
        subp.add_argument('--path', default='data', help='Output path'+dftstr)
        subp.add_argument('--depth', dest='depth', default=0, type=int, help='Buffer depth'+dftstr) 

        subp = subparsers.add_parser('dump', help='Dump buffer content')    
        addCommonArguments(subp)
        subp.add_argument('--path', default='data', help='Output path'+dftstr)
        subp.add_argument('--depth', dest='depth', default=0, type=int, help='Buffer depth'+dftstr) 
    
        subp = subparsers.add_parser('checkttc', help='Check TTC block status')
        addCommonArguments(subp)
        
        subp = subparsers.add_parser('ttcscan', help='Scans the TTC phase')
        addCommonArguments(subp)

        subp = subparsers.add_parser('ttccapture', help='Capture ttc commands')
        addCommonArguments(subp)
        subp.add_argument('--maskbc0', default=False, action='store_true', help='Skip BC0s'+dftstr )
        subp = subparsers.add_parser('minipods', help='print minipod sensor info')
        addCommonArguments(subp)
        
        subp = subparsers.add_parser('scansd', help='scan microsd card for files')
        addCommonArguments(subp)

        subp = subparsers.add_parser('uploadfw', help='upload firmware image to uSD')
        addCommonArguments(subp)
        subp.add_argument('--fwpath', dest='fwpath', default="", help='Path to image file to upload to uSD')
        subp.add_argument('--fwfile', dest='fwfile', default="", help='Name to give firmware image on uSD card')
        
        subp = subparsers.add_parser('setdummysensor', help='set dummy sensor value for ipmi tests')
        addCommonArguments(subp)
        subp.add_argument('--value', dest='dummyval', default=-128, type=int, help='Value for dummy sensor')
        
        subp = subparsers.add_parser('rebootfpga', help='Reboot fpga and load specific firmware image')
        addCommonArguments(subp)
        subp.add_argument('--fwrbfile', dest='fwrbfile', default="", help='Preloaded firmware image to load after fpga reboot')

        subp = subparsers.add_parser('hardreset', help='Hard reset the board')
        addCommonArguments(subp)
        
        subp = subparsers.add_parser('deleteimage', help='Delete firmware image from uSD card')
        addCommonArguments(subp)
        subp.add_argument('--delfile', dest='delfile', default="", help='Image filename to delete from uSD card')

        subp = subparsers.add_parser('downloadimage', help='Download firmware image from uSD card to path supplied on local disk')
        addCommonArguments(subp)
        subp.add_argument('--fwpath', dest='fwpath', default="", help='Path to download firmware image from uSD to local disk')
        subp.add_argument('--fwfile', dest='fwfile', default="", help='Firmware image filename to download from uSD card')

        return parser

    @classmethod
    def parseArguments(cls, argv=None ):
        '''to be cleaned up'''
    
        # print(argv, sys.argv)
        ns = cls.get().parse_args(argv)
    
        # validation if needed
        if ns.verbose == 0:
            ns.loglevel = logging.INFO
        else:
            ns.loglevel = logging.DEBUG
        
        return ns

def execute( argv=None ):
    if not argv: logging.info('Executing %s', ' '.join(sys.argv))

    args = ButlerParser.parseArguments(argv)
    # print(args)
    
    if args.cmd == 'exec':
        import string
        import shlex
        import math

        # print(args.vars)
        basemap = dict([ v.split('=') for v in args.vars])


        with open(args.file) as macrofile:
            macroTmpl = string.Template(macrofile.read())


        ifmt = '%0'+str(int(math.log10(args.repeat))+1)+'d'
        print(ifmt)
        for k in xrange(args.repeat):
            varmap = { 'cycle': (ifmt % k) }
            varmap.update(basemap)

            macro = macroTmpl.substitute(**varmap)
            commands = macro.split('\n')

            if args.wait:
                time.sleep(args.wait)

            logging.info('>'*40)
            logging.info('Starting cycle %d/%d',k+1,args.repeat)
            logging.info('>'*40)
            for cmd in commands:
                if len(cmd) == 0 or cmd[0] == '#': continue

                logging.info('='*40)
                logging.info('Executing: %s',cmd)
                logging.info('='*40)
                    
                execute(shlex.split(cmd))
        return

    elif args.cmd == 'show':
        logging.info( 'Available devices in %s:',args.connections)
        for d in cm.getDevices():
            logging.info(  '    - %s',d )
        return
    
    board = cm.getDevice( args.board )
    # run a simple access test
    hlp.testAccess(board)

    if args.cmd == 'connect':
        # nothing else to do. 
        return
    
    board.setTimeoutPeriod( args.timeout )
        
#    mp7cls = getattr(mp7ctrl,args.classname)
    mp7cls = getattr(mp7ctrl,mp7ctrl.models[args.model])

    
    mp7controller = mp7cls(board,args.enablelinks)
    mp7controller.printId()
    
    if args.cmd == 'ipython':
        # print(args.links)
        # mp7controller.links = args.links
        return mp7controller
    
    elif args.cmd == 'reset':
        
        clkOpt = args.clkOpt
        extClk40 = (args.clksrc == 'external')
        bc0confmap = { 
            None: None,
            'external':0,
            'internal':1,
            }
        overrideBC0 = bc0confmap[args.overrideBC0]
        try:
            mp7controller.reset(clkOpt, extClk40,overrideBC0)
        except BaseException as e:
            logging.critical(e)
    
    elif args.cmd == 'mgts':

        if args.loopback or args.forcepattern :
            # Configure the MGTs in loopback mode, for self alignment.
            # Use a pattern generated in the Tx buffers to align the Rx channels
            if args.orbittag:
                mp7controller.setupTx2RxOrbitPattern()
            else:
                mp7controller.setupTx2RxPattern()
    
        if args.config:
            try:
                # Configure the GTHs in loopback mode
                mp7controller.configureLinks(args.loopback)
            except StandardError as e:
                logging.exception('Aaaaarg!')
                logging.critical(e)

        if args.align:
            try:
                mp7controller.alignLinks(args.secondary, args.orbittag, args.fixlatency)
            except StandardError as e:
                logging.critical(e)
                return -1

        if args.check:
            # Run some checks to make sure no errors were found
            try:
                mp7controller.checkLinks()
            except BaseException as e:
                logging.critical(e)
                return -1

    elif args.cmd == 'monitor':
        import datetime

        errcntr = 0
        errtime = []
        for i in xrange(args.repeat):
            if i is not 0:
                time.sleep(args.wait)
            try:
                logging.info('==> Running check %d',i)
                mp7controller.checkLinks()
            except StandardError as e:
                errcntr += 1
                errtime.append(datetime.datetime.now())
                logging.error(e)
            if args.clear:
                mp7controller.clearCounters()

        elog = logging.info if errcntr==0 else logging.error
        elog('Summary: %d checks failed',errcntr)
        for i,t in enumerate(errtime):
            elog( ' -> Fail #%i timestamp: %s', i, str(t) )

    elif args.cmd == 'clocks':
        mp7controller.checkTTC()
        mp7controller.measureRefClocks()

    # elif args.cmd == 'header':
    #     mp7controller.configureHdrFormatter(args.strip, args.insert)

    elif args.cmd == 'buffers':
        from os.path import join
        mp7controller.buf.setSize(args.depth) 
        depth    = mp7controller.buf.getSize() 

        logging.warn('playback=%s, capture=%s, depth=%s' % (args.play_bx, args.cap_bx, depth) )
        
        # Prepare the data to be loaded into the pp RAMs
        if args.data_uri is not None:
            src = BoardDataSource( args.data_uri, depth=depth )
            # 2 preprocessors
            data =  src.data[0:depth]
        else:
            data = None
    
        # Instantiate a buffer configurator.
        mp7Configurator = Configurator.withBXRange( mp7controller, args.mode, playbx=args.play_bx, capbx=args.cap_bx )
        
        # Apply the selected configuration
        mp7Configurator.configure(mp7controller)
        
        # Pre-fill the buffers according to the selected operation mode
        data_rx, data_tx = mp7Configurator.assignRxTx(data)

        # configure the formatter
        mp7controller.configureHdrFormatter(args.strip, args.insert)
    
        # Forcefully clear buffers and load patters, if necessary
        mp7controller.clearBuffers( data_rx, data_tx, forceclear=True )

        # Reset algo block, to make sure they are not affected by the buffer configuration
        mp7controller.resetPayload()

    elif args.cmd == 'capture':
        from os.path import join
        
        mp7controller.buf.setSize(args.depth) 
        depth    = mp7controller.buf.getSize() 
        
        # print(data, data_rx, data_tx)
        mp7controller.captureAndDump( join(args.path) )

    elif args.cmd == 'dump': 
        from os.path import join
        
        mp7controller.buf.setSize(args.depth) 
        depth    = mp7controller.buf.getSize() 
        
        # print(data, data_rx, data_tx)
        mp7controller.dump( join(args.path) )

    elif args.cmd == 'checkttc':
        mp7controller.checkTTC()
        
    elif args.cmd == 'ttcscan':
        mp7controller.scanTTCPhase()

    elif args.cmd == 'ttccapture':
    
        history =  mp7controller.ttc.captureBGOs( args.maskbc0 )
    
        for i,v in enumerate(history):
            # Decoded values
            # valid = (v >> 31) & 1;
            # isL1A = (v >> 24) & 1;
            # orbit = (v >> 32) & 0xffffff;
            # bx    = (v >> 8) & 0xfff;
            # cmd   = (v & 0xff);
        
            valid = (v>>31) & 1
            cmd   =  v & 0xf
            bx    = (v>>4) & 0xfff
            l1a   = (v>>16) & 0x1
            orbit = (v>>32) & 0xffffff 

            print('%4d | V:%1d L1A:%1d orb:0x%06x bx:0x%03x cmd:%02x' % (i,valid, isL1A, orbit, bx, cmd))
    
    elif args.cmd == 'minipods':
        logging.info("Printing MiniPOD sensor info...")
        mp7controller.minipodSensors()

    elif args.cmd == 'scansd':
        logging.info("Scanning MicroSD card...")
        mp7controller.scanSD()

    elif args.cmd == 'uploadfw':
        if not args.fwpath or not args.fwfile or not os.path.isfile(args.fwpath):
            logging.error('No filepath or filename given, or file does not exist')
        else:
            logging.info("Uploading firmware image %s to uSD card..." % args.fwfile)
            mp7controller.fileToSD(args.fwpath, args.fwfile)

    elif args.cmd == 'setdummysensor':
        if not args.dummyval or (args.dummyval < -128 or args.dummyval > 127): 
            logging.error('Invalid or no dummy sensor value given (range: -128,127). Defaulting to 0xff (-1)')
            args.dummyval=0xff
        else:
            logging.info("Setting dummy sensor to value: %d" % args.dummyval)
            mp7controller.setDummySensor(args.dummyval)
    
    elif args.cmd == 'rebootfpga':
        if not args.fwrbfile:
            logging.error('No firmware image given. Add --fwrbfile="". Listing image names on uSD...')
            mp7controller.scanSD()
        else:
            logging.info("Rebooting FPGA with firmware image %s..." %args.fwrbfile)
            uhal.setLogLevelTo(uhal.LogLevel.FATAL)
            mp7controller.rebootFPGA(args.fwrbfile)
            uhal.setLogLevelTo(uhal.LogLevel.ERROR)

    elif args.cmd == 'hardreset':
        logging.info("Hard reset of the board...")
        mp7controller.hardReset()

    elif args.cmd == 'deleteimage':
        if not args.delfile:
            logging.error('No firmware image filename given')
        else:
            logging.info("Deleting image %s from uSD card..." % args.delfile)
            mp7controller.deleteFromSD(args.delfile)
    elif args.cmd == 'downloadimage':
        if not args.fwpath or not args.fwfile:
            logging.error('No firmware image filename or path given')
        else:
            logging.info('Downloading image %s from uSD card to %s' % (args.fwfile, args.fwpath))
            mp7controller.fileFromSD(args.fwpath,args.fwfile)

args = ButlerParser.parseArguments()

# print('verbose',args.verbose, 'loglevel',args.loglevel)
# logging initialization
hlp.initLogging( args.loglevel, args.log )

# Ads
if not args.quiet:
    hlp.mp7Logo()

    print('-'*40)
    print('MP7 Butler: One script to rule them all')
    print('-'*40)
    print()

# Mask the errors when loading the address tables
# uhal.setLogLevelTo(uhal.LogLevel.ERROR)
uhal.setLogLevelTo(uhal.LogLevel.WARNING)
# uhal.setLogLevelTo(uhal.LogLevel.DEBUG)
cm = uhal.ConnectionManager(args.connections)

retval = execute()
if retval:
    logging.info('Execute returned something: %s',retval)
