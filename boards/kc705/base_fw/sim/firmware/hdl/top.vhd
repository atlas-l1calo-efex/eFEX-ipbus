-- Top-level design for MP7 base firmware
--
-- Dave Newbold, July 2012

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.ipbus.all;
use work.ipbus_trans_decl.all;
use work.mp7_data_types.all;
use work.mp7_readout_decl.all;
use work.mp7_ttc_decl.all;
use work.top_decl.all;

entity top is

-- No ports here

end top;

architecture rtl of top is

	signal clk_ipb, rst_ipb, clk160, rst160, clk240, rst240, clk40, rst40: std_logic;
	signal clk40_rst, clk40_sel, clk40_lock, clk40_stop, nuke, soft_rst: std_logic;
	signal clk_p, rst_p: std_logic;

	signal ipb_in_ctrl, ipb_in_ttc, ipb_in_datapath, ipb_in_readout, ipb_in_payload, ipb_in_formatter: ipb_wbus;
	signal ipb_out_ctrl, ipb_out_ttc, ipb_out_datapath, ipb_out_readout, ipb_out_payload, ipb_out_formatter: ipb_rbus;

	signal payload_d, payload_q, formatter_d, formatter_q: ldata(N_REGION * 4	- 1 downto 0);
	signal qsel: std_logic_vector(7 downto 0);
	signal ttc_l1a, dist_lock: std_logic;
	signal ttc_cmd, ttc_cmd_dist: std_logic_vector(3 downto 0);
	signal bunch_ctr: std_logic_vector(11 downto 0);
	signal evt_ctr, orb_ctr: std_logic_vector(23 downto 0);

	signal clkmon: std_logic_vector(2 downto 0);

	signal cap_bus: daq_cap_bus;
	signal daq_bus_top, daq_bus_bot: daq_bus;
	signal ctrs: ttc_stuff_array(N_REGION - 1 downto 0);
	signal rst_loc: std_logic_vector(N_REGION - 1 downto 0);

begin

-- Clocks and control IO

	infra: entity work.mp7_infra_sim
		generic map(
			MAC_ADDR => X"000A3501EDF0",
			IP_ADDR => X"c0a8c902"
		)
		port map(
			clk_ipb => clk_ipb,
			rst_ipb => rst_ipb,
			nuke => nuke,
			soft_rst => soft_rst,			
			ipb_in_ctrl => ipb_out_ctrl,
			ipb_out_ctrl => ipb_in_ctrl,
			ipb_in_ttc => ipb_out_ttc,
			ipb_out_ttc => ipb_in_ttc,
			ipb_in_datapath => ipb_out_datapath,
			ipb_out_datapath => ipb_in_datapath,
			ipb_in_readout => ipb_out_readout,
			ipb_out_readout => ipb_in_readout,
			ipb_in_payload => ipb_out_payload,
			ipb_out_payload => ipb_in_payload,
			ipb_in_formatter => ipb_out_formatter,
			ipb_out_formatter => ipb_in_formatter
		);

-- Control registers
		
	ctrl: entity work.mp7_ctrl
		generic map(
			FW_REV => X"11010400"
		)
		port map(
			clk => clk_ipb,
			rst => rst_ipb,
			ipb_in => ipb_in_ctrl,
			ipb_out => ipb_out_ctrl,
			nuke => nuke,
			soft_rst => soft_rst,			
			qsel => qsel,
			clk40_rst => clk40_rst,
			clk40_sel => clk40_sel,
			clk40_lock => clk40_lock,
			clk40_stop => clk40_stop
		);	

-- TTC signal handling		
	
	ttc: entity work.mp7_ttc_sim
		port map(
			clk => clk_ipb,
			rst => rst_ipb,
			mmcm_rst => clk40_rst,
			sel => clk40_sel,
			lock => clk40_lock,
			stop => clk40_stop,
			ipb_in => ipb_in_ttc,
			ipb_out => ipb_out_ttc,
			clk40 => clk40,
			rsto40 => rst40,
			clk160 => clk160,
			rsto160 => rst160,
			clk240 => clk240,
			rsto240 => rst240,
			ttc_cmd => ttc_cmd,
			ttc_cmd_dist => ttc_cmd_dist,
			ttc_l1a => ttc_l1a,
			dist_lock => dist_lock,
			bunch_ctr => bunch_ctr,
			evt_ctr => evt_ctr,
			orb_ctr => orb_ctr,
			monclk => clkmon
		);
		
	clk_p <= clk240 when CLOCK_RATIO = 6 else clk160;
	rst_p <= rst240 when CLOCK_RATIO = 6 else rst160;

-- MGTs, buffers and TTC fanout
		
	datapath: entity work.mp7_datapath_2
		port map(
			clk => clk_ipb,
			rst => rst_ipb,
			ipb_in => ipb_in_datapath,
			ipb_out => ipb_out_datapath,
			qsel => qsel,
			clk40 => clk40,
			clk_p => clk_p,
			rst_p => rst_p,
			ttc_cmd => ttc_cmd_dist,
			lock => dist_lock,
			ctrs_out => ctrs,
			rst_out => rst_loc,
			cap_bus => cap_bus,
			daq_bus_in => daq_bus_top,
			daq_bus_out => daq_bus_bot,
			refclkp => (others => '0'),
			refclkn => (others => '0'),
			clkmon => clkmon,
			q => formatter_d,
			d => formatter_q
		);

-- Readout
		
	readout: entity work.mp7_readout
		port map(
			clk => clk_ipb,
			rst => rst_ipb,
			ipb_in => ipb_in_readout,
			ipb_out => ipb_out_readout,
			clk40 => clk40,
			rst40 => rst40,
			ttc_cmd => ttc_cmd,
			l1a => ttc_l1a,
			bunch_ctr => bunch_ctr,
			evt_ctr => evt_ctr,
			orb_ctr => orb_ctr,
			clk_p => clk_p,
			rst_p => rst_p,
			cap_bus => cap_bus,
			daq_bus_out => daq_bus_top,
			daq_bus_in => daq_bus_bot
		);

-- Formatter

	formatter: entity work.mp7_formatter
		port map(
			clk => clk_ipb,
			rst => rst_ipb,
			ipb_in => ipb_in_formatter,
			ipb_out => ipb_out_formatter,		
			clk_p => clk_p,
			rsts_p => rst_loc,
			ctrs => ctrs,
			d_buf => formatter_d,
			q_payload => payload_d,
			d_payload => payload_q,
			q_buf => formatter_q
		);

-- Payload
		
	payload: entity work.mp7_payload
		generic map(
			N_REGION => N_REGION,
			PIPELINE_STAGES => 2
		)
		port map(
			clk => clk_ipb,
			rst => rst_ipb,
			ipb_in => ipb_in_payload,
			ipb_out => ipb_out_payload,			
			clk_p => clk_p,
			d => payload_d,
			q => payload_q
		);
		
end rtl;

