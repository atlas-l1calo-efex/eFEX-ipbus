# Generates a mp7 beased v1_4_0 project form kc502
# Nikitas.Loukas@cern.ch

project open top
source    ../cactusupgrades/boards/kc705/base_fw/kc705/firmware/cfg/kc705.tcl
source    ../cactusupgrades/boards/kc705/base_fw/common/firmware/cfg/settings_v7.tcl
xfile remove mp7_xpoint.vhd
xfile remove xpoint.vhd
xfile remove ipbus_decode_mp7_xpoint.vhd
xfile remove ipbus_i2c_master.vhd
xfile remove i2c_master_top.vhd
xfile remove i2c_master_bit_ctrl.vhd
xfile remove i2c_master_byte_ctrl.vhd
xfile remove i2c_master_registers.vhd
xfile remove uc_spi_interface.vhd
xfile remove mezzanine_out_lvds.vhd
xfile remove uc_if.vhd
xfile remove trans_buffer.vhd
xfile remove uc_pipe_interface.vhd
xfile remove sdpram_16x10_32x9.xco
xfile remove sdpram_32x9_16x10.xco
xfile remove mp7_690es.vhd
xfile add ../cactusupgrades/boards/kc705/base_fw/kc705/firmware/hdl/kc705_top.vhd
xfile remove top_decl.vhd
xfile add ../cactusupgrades/boards/kc705/base_fw/kc705/firmware/hdl/top_decl.vhd
xfile remove mp7_infra.vhd
xfile add ../cactusupgrades/boards/kc705/base_fw/common/firmware/hdl/kc705_infra.vhd
xfile remove eth_7s_1000basex_gth.vhd
xfile add ../cactusupgrades/boards/kc705/base_fw/common/firmware/hdl/eth_7s_1000basex.vhd
xfile remove mp7_ctrl.vhd
xfile add ../cactusupgrades/boards/kc705/base_fw/common/firmware/hdl/kc705_ctrl.vhd
xfile remove ext_align_gth_32b_10g_spartan.vhd
xfile add ../cactusupgrades/boards/kc705/base_fw/common/firmware/hdl/ext_align_gth_32b_10g_spartan.vhd
xfile remove gth_quad_wrapper_8b10bx32b.ngc
xfile add ../cactusupgrades/boards/kc705/base_fw/common/firmware/ngc/gtx_quad_wrapper_8b10bx32b.ngc
xfile remove mp7_null_algo.ucf
xfile add ../cactusupgrades/boards/kc705/base_fw/common/firmware/ucf/kc705_null_algo.ucf
xfile remove area_constraints.ucf
xfile add ../cactusupgrades/boards/kc705/base_fw/common/firmware/ucf/area_constraints.ucf
xfile remove clock_constraints.ucf
xfile add ../cactusupgrades/boards/kc705/base_fw/common/firmware/ucf/clock_constraints.ucf
xfile remove mp7_formatter.ucf
xfile add ../cactusupgrades/boards/kc705/base_fw/common/firmware/ucf/kc705_formatter.ucf
xfile remove mp7_pins.ucf
xfile add ../cactusupgrades/boards/kc705/base_fw/kc705/firmware/ucf/kc705_pins.ucf
xfile remove mp7.ucf
xfile add ../cactusupgrades/boards/kc705/base_fw/kc705/firmware/ucf/kc705.ucf
xfile remove gig_eth_pcs_pma_v11_5_block.vhd
xfile remove gig_eth_pcs_pma_v11_5.xco
xfile remove gig_eth_pcs_pma_v11_5_transceiver_gth.vhd
xfile remove gig_eth_pcs_pma_v11_5_reset_sync.vhd
xfile remove gtwizard_v2_5_gbe_gth_init.vhd
xfile remove gtwizard_v2_5_gbe_gth.vhd
xfile remove gtwizard_v2_5_gbe_gth_gt.vhd
xfile remove gtwizard_v2_5_gbe_gth_gtrxreset_seq.vhd
xfile remove gtwizard_v2_5_gbe_gth_sync_block.vhd
xfile remove gtwizard_v2_5_gbe_gth_tx_startup_fsm.vhd
xfile remove gtwizard_v2_5_gbe_gth_rx_startup_fsm.vhd
exec mkdir -p ipcore_dir
exec cp ../../kc705/base_fw/kc705/firmware/cfg/coregen.cgp ipcore_dir
exec cp ../cactusupgrades/components/ipbus_eth/firmware/cgn/gig_eth_pcs_pma_v11_4.xco ipcore_dir
cd ipcore_dir
exec coregen -r -b gig_eth_pcs_pma_v11_4.xco -p coregen.cgp >& coregen.out
cd ..
xfile add ipcore_dir/gig_eth_pcs_pma_v11_4.xco
xfile add ipcore_dir/gig_eth_pcs_pma_v11_4/example_design/gig_eth_pcs_pma_v11_4_sync_block.vhd
xfile add ipcore_dir/gig_eth_pcs_pma_v11_4/example_design/gig_eth_pcs_pma_v11_4_reset_sync.vhd
xfile add ipcore_dir/gig_eth_pcs_pma_v11_4/example_design/gig_eth_pcs_pma_v11_4_block.vhd
xfile add ../cactusupgrades/components/ipbus_eth/firmware/gen_hdl/gig_eth_pcs_pma_v11_4/gig_eth_pcs_pma_v11_4_transceiver.vhd
xfile add ../cactusupgrades/components/ipbus_eth/firmware/gen_hdl/gtwizard_v2_3_gbe/gtwizard_v2_3_gbe_init.vhd
xfile add ../cactusupgrades/components/ipbus_eth/firmware/gen_hdl/gtwizard_v2_3_gbe/gtwizard_v2_3_gbe.vhd
xfile add ../cactusupgrades/components/ipbus_eth/firmware/gen_hdl/gtwizard_v2_3_gbe/gtwizard_v2_3_gbe_gt.vhd
xfile add ../cactusupgrades/components/ipbus_eth/firmware/gen_hdl/gtwizard_v2_3_gbe/gtwizard_v2_3_gbe_tx_startup_fsm.vhd
xfile add ../cactusupgrades/components/ipbus_eth/firmware/gen_hdl/gtwizard_v2_3_gbe/gtwizard_v2_3_gbe_rx_startup_fsm.vhd
xfile add ../cactusupgrades/components/ipbus_eth/firmware/gen_hdl/gtwizard_v2_3_gbe/gtwizard_v2_3_gbe_recclk_monitor.vhd
process run "Regenerate Core" -instance ttc_history_fifo
process run "Regenerate Core" -instance virtex7_rx_buf
project set top rtl top
project close

