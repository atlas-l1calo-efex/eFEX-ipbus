-- Top-level design for MP7 base firmware
--
-- Dave Newbold, July 2012
-- Nikitas Loukas, November 2014 (Keeping only the non mp7-specific parts, fixedIP:192.168.1.99)

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.ipbus.all;
use work.ipbus_trans_decl.all;
use work.mp7_data_types.all;
use work.mp7_readout_decl.all;
use work.mp7_ttc_decl.all;
use work.top_decl.all;

entity top is
	port(
		eth_clkp, eth_clkn: in std_logic;
		eth_txp, eth_txn: out std_logic;
		eth_rxp, eth_rxn: in std_logic;
		leds_gpoi: out std_logic_vector(7 downto 0);
		clk40_in_p: in std_logic;
		clk40_in_n: in std_logic;
		ttc_in_p: in std_logic;
		ttc_in_n: in std_logic;
		refclkp: in std_logic_vector(N_REFCLK - 1 downto 0);
		refclkn: in std_logic_vector(N_REFCLK - 1 downto 0)
---- loopbacks using the FMC105
--	fmc1_la_p : in std_logic_vector(0 to 31);
--	fmc1_la_n : in std_logic_vector(0 to 31);
--	fmc1_ha_p : in std_logic_vector(0 to 19);
--	fmc1_ha_n : in std_logic_vector(0 to 19);
--	fmc1_hb_p : in std_logic_vector(0 to 19);
--	fmc1_hb_n : in std_logic_vector(0 to 19);
--	sfp_nena_dp : out std_logic_vector(3 downto 0); -- tx_disable signals
--	sfp_rate_dp : out std_logic_vector(3 downto 0) -- rate_sel signals
	);

end top;

architecture rtl of top is
	
	signal clk_ipb, rst_ipb, clk40ish, clk160, rst160, clk240, rst240, clk40, rst40: std_logic;
	signal clk40_rst, clk40_sel, clk40_lock, clk40_stop, nuke, soft_rst: std_logic;
	signal clk_p, rst_p: std_logic;
	
	signal si5326_sda_o: std_logic;
	
	signal ipb_in_ctrl, ipb_in_ttc, ipb_in_datapath, ipb_in_readout, ipb_in_payload, ipb_in_formatter: ipb_wbus;
	signal ipb_out_ctrl, ipb_out_ttc, ipb_out_datapath, ipb_out_readout, ipb_out_payload, ipb_out_formatter: ipb_rbus;
	
	signal payload_d, payload_q, formatter_d, formatter_q: ldata(N_REGION * 4	- 1 downto 0);
	signal qsel: std_logic_vector(7 downto 0);
	signal ttc_l1a, dist_lock, oc_flag, ec_flag: std_logic;
	signal ttc_cmd, ttc_cmd_dist: ttc_cmd_t;
	signal bunch_ctr: std_logic_vector(11 downto 0);
	signal evt_ctr, orb_ctr: std_logic_vector(23 downto 0);
	
	signal clkmon: std_logic_vector(2 downto 0);
	
	signal cap_bus: daq_cap_bus;
	signal daq_bus_top, daq_bus_bot: daq_bus;
	signal ctrs: ttc_stuff_array(N_REGION - 1 downto 0);
	signal rst_loc: std_logic_vector(N_REGION - 1 downto 0);

	signal leds: std_logic_vector(11 downto 0);

begin

-----hpc-enable-----------------------
--	sfp_nena_dp <= (others => '0'); -- tx_disable signals
--	sfp_rate_dp <= (others => '0'); -- rate_sel signals

-- comments --
--	leds <= '1' & not led_q(1) & not led_q(0) & "111" & '1' & not (locked and onehz) & locked & '1' & not led_q(3) & not led_q(2);
	leds_gpoi <= '0' & '0' & leds(4) & leds(3) & not leds(1) & not leds(0) & not leds(10) & not leds(9);
--	                         onehz     locked        led_q(3)      led_q(2)      led_qeds(1)    led_q(0)
--------------

-- Clocks and control IO

	infra: entity work.mp7_infra
		port map(
			gt_clkp => eth_clkp,
			gt_clkn => eth_clkn,
			gt_txp => eth_txp,
			gt_txn => eth_txn,
			gt_rxp => eth_rxp,
			gt_rxn => eth_rxn,
			leds => open, --leds,
			clk_ipb => clk_ipb,
			rst_ipb => rst_ipb,
			clk40ish => clk40ish,
			nuke => nuke,
			soft_rst => soft_rst,
			oc_flag => oc_flag,
			ec_flag => ec_flag,
			ipb_in_ctrl => ipb_out_ctrl,
			ipb_out_ctrl => ipb_in_ctrl,
			ipb_in_ttc => ipb_out_ttc,
			ipb_out_ttc => ipb_in_ttc,
			ipb_in_datapath => ipb_out_datapath,
			ipb_out_datapath => ipb_in_datapath,
			ipb_in_readout => ipb_out_readout,
			ipb_out_readout => ipb_in_readout,
			ipb_in_payload => ipb_out_payload,
			ipb_out_payload => ipb_in_payload,
			ipb_in_formatter => ipb_out_formatter,
			ipb_out_formatter => ipb_in_formatter
		);

-- Control registers and board IO
		
	ctrl: entity work.mp7_ctrl
		generic map(
			FW_REV => X"10010400"
		)
		port map(
			clk => clk_ipb,
			rst => rst_ipb,
			ipb_in => ipb_in_ctrl,
			ipb_out => ipb_out_ctrl,
			nuke => nuke,
			soft_rst => soft_rst,
			qsel => qsel,
			clk40_rst => clk40_rst,
			clk40_sel => clk40_sel,
			clk40_lock => clk40_lock,
			clk40_stop => clk40_stop
			);
		
-- TTC signal handling
	
	ttc: entity work.mp7_ttc
		port map(
			clk => clk_ipb,
			rst => rst_ipb,
			mmcm_rst => clk40_rst,
			sel => clk40_sel,
			lock => clk40_lock,
			stop => clk40_stop,
			ipb_in => ipb_in_ttc,
			ipb_out => ipb_out_ttc,
			clk40_in_p => clk40_in_p,
			clk40_in_n => clk40_in_n,
			clk40ish_in => clk40ish,
			clk40 => clk40,
			rsto40 => rst40,
			clk160 => clk160,
			rsto160 => rst160,
			clk240 => clk240,
			rsto240 => rst240,
			ttc_in_p => ttc_in_p,
			ttc_in_n => ttc_in_n,
			ttc_cmd => ttc_cmd,
			ttc_cmd_dist => ttc_cmd_dist,
			ttc_l1a => ttc_l1a,
			dist_lock => dist_lock,
			bunch_ctr => bunch_ctr,
			evt_ctr => evt_ctr,
			orb_ctr => orb_ctr,
			oc_flag => oc_flag,
			ec_flag => ec_flag,
			monclk => clkmon
		);
		
		clk_p <= clk240 when CLOCK_RATIO = 6 else clk160;
		rst_p <= rst240 when CLOCK_RATIO = 6 else rst160;

-- MGTs, buffers and TTC fanout
		
	datapath: entity work.mp7_datapath_2
		port map(
			clk => clk_ipb,
			rst => rst_ipb,
			ipb_in => ipb_in_datapath,
			ipb_out => ipb_out_datapath,
			qsel => qsel,
			clk40 => clk40,
			clk_p => clk_p,
			rst_p => rst_p,
			ttc_cmd => ttc_cmd_dist,
			lock => dist_lock,
			ctrs_out => ctrs,
			rst_out => rst_loc,
			cap_bus => cap_bus,
			daq_bus_in => daq_bus_top,
			daq_bus_out => daq_bus_bot,
			refclkp => refclkp,
			refclkn => refclkn,
			clkmon => clkmon,
			q => formatter_d,
			d => formatter_q
		);

-- Readout
		
	readout: entity work.mp7_readout
		port map(
			clk => clk_ipb,
			rst => rst_ipb,
			ipb_in => ipb_in_readout,
			ipb_out => ipb_out_readout,
			clk40 => clk40,
			rst40 => rst40,
			ttc_cmd => ttc_cmd,
			l1a => ttc_l1a,
			bunch_ctr => bunch_ctr,
			evt_ctr => evt_ctr,
			orb_ctr => orb_ctr,			
			clk_p => clk_p,
			rst_p => rst_p,
			cap_bus => cap_bus,
			daq_bus_out => daq_bus_top,
			daq_bus_in => daq_bus_bot
		);

-- Formatter

	formatter: entity work.mp7_formatter
		port map(
			clk => clk_ipb,
			rst => rst_ipb,
			ipb_in => ipb_in_formatter,
			ipb_out => ipb_out_formatter,		
			clk_p => clk_p,
			rsts_p => rst_loc,
			ctrs => ctrs,
			d_buf => formatter_d,
			q_payload => payload_d,
			d_payload => payload_q,
			q_buf => formatter_q
		);		

-- Payload
		
	payload: entity work.mp7_payload
		generic map(
			N_REGION => N_REGION,
			PIPELINE_STAGES => 2
		)
		port map(
			clk => clk_ipb,
			rst => rst_ipb,
			ipb_in => ipb_in_payload,
			ipb_out => ipb_out_payload,			
			clk_p => clk_p,
			d => payload_d,
			q => payload_q
		);

end rtl;
