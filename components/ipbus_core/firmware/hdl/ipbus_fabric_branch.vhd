---------------------------------------------------------------------------------
--
--   Copyright 2017 - Rutherford Appleton Laboratory and University of Bristol
--
--   Licensed under the Apache License, Version 2.0 (the "License");
--   you may not use this file except in compliance with the License.
--   You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--   Unless required by applicable law or agreed to in writing, software
--   distributed under the License is distributed on an "AS IS" BASIS,
--   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--   See the License for the specific language governing permissions and
--   limitations under the License.
--
--                                     - - -
--
--   Additional information about ipbus-firmare and the list of ipbus-firmware
--   contacts are available at
--
--       https://ipbus.web.cern.ch/ipbus
--
---------------------------------------------------------------------------------


-- The ipbus bus fabric, address select logic, data multiplexers
--
-- This version just does a flat address decode according to some specified
-- bits in the address
--
-- Dave Newbold, February 2011

-- Richard Staley, June 2015. 
-- Modified ipbus_fabric_simple(Dave Newbold). DECODE_BITS now defined by NSLV
-- Address decoding for N blocks each with DECODE_BASE address bits.
--
-- Updated to reflect true ack and err in case of misbehaving slave...
--
-- Dave Sankey April 2021

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
USE ieee.math_real.all;
use work.ipbus.ALL;

entity ipbus_fabric_branch is
  generic(
    NSLV: positive;
    STROBE_GAP: boolean := false;
    DECODE_BASE: natural
   );
  port(
    ipb_in: in ipb_wbus;
    ipb_out: out ipb_rbus;
    ipb_to_slaves: out ipb_wbus_array(NSLV - 1 downto 0);
    ipb_from_slaves: in ipb_rbus_array(NSLV - 1 downto 0) := (others => IPB_RBUS_NULL)
   );

end ipbus_fabric_branch;

architecture rtl of ipbus_fabric_branch is

	constant DECODE_BITS	: integer := integer(ceil(log2(real(NSLV))));

	signal sel: integer range 0 to 2 ** DECODE_BITS - 1 := 0;
	signal true_ack, true_err, qstrobe: std_logic;

begin

-- synthesis translate_off
printout: process(sel) is
    begin
    report "DECODE_BITS = " & integer'image(DECODE_BITS);
end process printout;
-- synthesis translate_on 

	sel <= to_integer(unsigned(ipb_in.ipb_addr(DECODE_BASE + DECODE_BITS-1 downto DECODE_BASE)));
	
	qstrobe <= ipb_in.ipb_strobe when STROBE_GAP = false else
	 ipb_in.ipb_strobe and not (true_ack or true_err);

	busgen: for i in NSLV-1 downto 0 generate
	begin

		ipb_to_slaves(i).ipb_addr <= ipb_in.ipb_addr;
		ipb_to_slaves(i).ipb_wdata <= ipb_in.ipb_wdata;
		ipb_to_slaves(i).ipb_strobe <= qstrobe when sel = i else '0';
		ipb_to_slaves(i).ipb_write <= ipb_in.ipb_write;

	end generate;

  true_ack <= ipb_from_slaves(sel).ipb_ack when sel /= NSLV else '0';
  true_err <= ipb_from_slaves(sel).ipb_err when sel /= NSLV else '0';
  ipb_out.ipb_rdata <= ipb_from_slaves(sel).ipb_rdata when sel /= NSLV else (others => '0');
  ipb_out.ipb_ack <= true_ack;
  ipb_out.ipb_err <= true_err;
  
end rtl;

